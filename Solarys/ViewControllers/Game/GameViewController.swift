//
//  GameViewController.swift
//  Solarys
//
//  Created by Marcello De Palo on 11/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

class GameViewController: UIViewController {

    // MARK: - @IBOutlet

    @IBOutlet fileprivate weak var collectionView: UICollectionView!
    
    // MARK: - Private

    fileprivate lazy var progressView: ProgressView = {
        let view = ProgressView(frame: .zero)
        view.defaultStyle = .linear
        view.backgroundColor = UIColor.from(hexString: "#EEEEEE")
        return view
    }()
    
    fileprivate var viewModel: GameViewModel!

    // MARK: - Public
    
    // MARK: - Lifecycle

    convenience init(viewModel: GameViewModel) {
        self.init(nibName: nil, bundle: nil)
        self.viewModel = viewModel
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        progressView.frame = CGRect(x: 0, y: 0, width: 300, height: 15)
        progressView.center = self.view.center
        progressView.progress = CGFloat(viewModel.percentage)
        
        navigationItem.titleView = progressView
        navigationController?.setNavigationBarHidden(false, animated: false)
        
        bindToViewModel()
        reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if #available(iOS 11, *) {
            collectionView.contentInsetAdjustmentBehavior = .never
        }
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)

        coordinator.animate(alongsideTransition: { (context) in
            self.collectionView.collectionViewLayout.invalidateLayout()
        }, completion: nil)
    }

    // MARK: - ViewModel
        
    fileprivate func bindToViewModel() {
        self.viewModel.didUpdate = { [weak self] in
            self?.viewModelDidUpdate()
        }
        self.viewModel.didError = { [weak self] error in
            self?.viewModelDidError(error)
        }
    }
    
    fileprivate func viewModelDidUpdate() {
        progressView.progress = CGFloat(viewModel.percentage)
    }
    
    fileprivate func viewModelDidError(_ error: Error) {
        
    }

    // MARK: Actions

    @objc fileprivate func reloadData() {
        viewModel.reloadData()
        viewModel.viewModelsTypes.forEach { $0.registerCell(self.collectionView) }
    }
}

// MARK: UICollectionViewDataSource

extension GameViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.viewModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = viewModel.viewModels[indexPath.item].dequeueCell(collectionView, cellForItemAt: indexPath)
        return cell ?? UICollectionViewCell()
    }
}

// MARK: UICollectionViewDelegate

extension GameViewController : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.viewModels[indexPath.item].selected()
    }
}

// MARK: UICollectionViewDelegateFlowLayout

extension GameViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return viewModel.viewModels[indexPath.item].size(forItemIn: collectionView.frame)
    }
}

extension GameViewController: NavigationThemeable {
    var navigationBarBackgroundColor: UIColor? { return .clear }
    var navigationBarTintColor: UIColor? { return UIColor.from(hexString: "#5E6E92") }
}
