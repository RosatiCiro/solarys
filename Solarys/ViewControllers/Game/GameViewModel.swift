//
//  GameViewModel.swift
//  Solarys
//
//  Created by Marcello De Palo on 11/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit
import CoreData

class GameViewModel {
    
    // MARK: - Private
    
    fileprivate let app: Application

    fileprivate let topic: Topic

    fileprivate(set) var viewModels = [CellRepresentable]()
        
    // MARK: - Properties

    var percentage: Double = 0
    
    var viewModelsTypes: [CellRepresentable.Type] = []
    
    // MARK: - Events
    
    var didError: ((Error) -> Void)?
    var didSelect: ((Topic) -> Void)?
    var didUpdate: (() -> Void)?
    var didUpdateProgress: (() -> Void)?
    var didComplete: ((Topic?) -> Void)?

    // MARK: - Lifeycle
    
    init(_ topic: Topic, app: Application) {
        self.topic = topic
        self.app = app
    }
        
    // MARK: - Actions
    
    func reloadData() {
        viewModels = topic.modules.compactMap { self.viewModelFor($0) }
    }
    
    // MARK: - Helpers
    
    fileprivate func viewModelFor(_ module: Module) -> CellRepresentable? {
        guard
            let cellRepresentable = module.cellRepresentable,
            var viewModel = cellRepresentable as? ViewModel
            else { return nil }
          
        // guard to exclude levels completed
        guard (!(module.as(LevelCellViewModel.self) != nil) || (isCompleted(module) == false))
            else {
                return nil
        }
        
        viewModel.didSelect = { [weak self] element in
            guard let `self` = self else { return }
            self.didSelect?(self.topic)
        }

        viewModel.didUpdate = { [weak self] item in
            guard let `self` = self else { return }
            
            if let update = item as? ApplicationUpdate {
                switch update {
                case .level(let level):
                    self.percentage = level.percentage
                    self.saveDataFor(level)
                    self.didUpdate?()
                case .levelDone:
                    self.didComplete?(nil)
                case .topicDone:
                    //self.saveDataFor(self.topic)
                    self.didComplete?(self.topic)
                default:
                    return;
                }
            }
        }

        viewModel.didError = { [weak self] error in
            self?.didError?(error)
        }
        
        viewModelsTypes.append(cellRepresentable.type)

        return cellRepresentable
    }
    
    fileprivate func isCompleted(_ module: Module) -> Bool {
        guard let level = module.as(LevelCellViewModel.self) else {
            return false
        }
        return level.isCompleted
    }
    
    fileprivate func saveDataFor(_ level: Level) {
        guard level.isComplete else { return }
        
        let data = [
            "story": level.story,
            "topic": level.topic,
            "level": level.level
        ] as Dictionary<String, AnyObject>
        DataManager.shared.store(data, for: "History")
        
        print(DataManager.shared.fetchHistory())
    }
        
    func reset() {
        topic.modules.forEach({ module in
            if let cellRepresentable = module.cellRepresentable,
                let level = cellRepresentable as? LevelCellViewModel {
                
                level.reset()

                DataManager.shared
                    .deleteHistory(story: level.story,
                                   topic: level.topic,
                                   level: level.level)
            }
        })
    }
}
