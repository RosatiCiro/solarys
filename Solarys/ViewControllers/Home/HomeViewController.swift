//
//  HomeViewController.swift
//  Solarys
//
//  Created by Marcello De Palo on 11/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    // MARK: - IBOutlets
    
    @IBOutlet private var collectionView: UICollectionView!

    // MARK: - Private
        
    fileprivate var viewModel: HomeViewModel!

    var storedOffsets = [Int: CGFloat]()

    // MARK: - Lifecycle
    
    convenience init(viewModel: HomeViewModel) {
        self.init(nibName: nil, bundle: nil)
        self.viewModel = viewModel
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .from(hexString: "#F1F1F1")
        navigationController?.setNavigationBarHidden(true, animated: false)
            
//        self.navigationItem.rightBarButtonItem = UIBarButtonItem(
//            title: "Refresh", style: .plain,
//            target: self, action: #selector(HomeViewController.reloadData)
//        )

        viewModel.storyViewModelsTypes.forEach { $0.registerCell(self.collectionView) }
        
        bindToViewModel()
        reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        reloadData()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - ViewModel
    
    fileprivate func bindToViewModel() {
        self.viewModel.didUpdate = { [weak self] _ in
            self?.viewModelDidUpdate()
        }
        self.viewModel.didError = { [weak self] error in
            self?.viewModelDidError(error)
        }
    }
    
    fileprivate func viewModelDidUpdate() {
        //self.navigationItem.rightBarButtonItem?.isEnabled = !self.viewModel.isUpdating
        collectionView.reloadData()
    }
    
    fileprivate func viewModelDidError(_ error: Error) {
    }
    
    // MARK: - Actions
    
    @objc fileprivate func reloadData() {
        viewModel.reloadData()
    }
}

// MARK: UICollectionViewDataSource

extension HomeViewController: UICollectionViewDataSource {
    
    final func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.storyViewModels.count
    }
    
    final func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = viewModel.storyViewModels[indexPath.item].dequeueCell(collectionView, cellForItemAt: indexPath)
        return cell ?? UICollectionViewCell()
    }
}

// MARK: UICollectionViewDelegate

extension HomeViewController : UICollectionViewDelegate {
    
    final func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.storyViewModels[indexPath.item].selected()
    }
    
    final func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let storyCell = cell as? StoryCell else { return }
        
        storyCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
    }

    final func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let storyCell = cell as? StoryCell else { return }

        storedOffsets[indexPath.row] = storyCell.collectionViewOffset
    }
}

// MARK: UICollectionViewDelegateFlowLayout

extension HomeViewController : UICollectionViewDelegateFlowLayout {
    
    final func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return viewModel
            .storyViewModels[indexPath.item]
            .size(forItemIn: collectionView.frame)
    }
}

// MARK: - Scroll View Delegate

extension HomeViewController: UIScrollViewDelegate {

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        viewModel.storyViewModels.forEach({ element in
            element.scrollToItem = false
        })
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        viewModel.storyViewModels.forEach({ element in
            //element.scrollToItem = false
        })
    }
}

extension HomeViewController {
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)

        coordinator.animate(alongsideTransition: { (context) in
            self.collectionView.collectionViewLayout.invalidateLayout()
        }, completion: nil)
    }
}

extension HomeViewController: NavigationThemeable {
    var navigationBarBackgroundColor: UIColor? { return .clear }
    var navigationBarTintColor: UIColor? { return UIColor.white }
}
