//
//  HomeViewModel.swift
//  Solarys
//
//  Created by Marcello De Palo on 11/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import Foundation
import UIKit

class HomeViewModel {
    
    // MARK: - Events
    
    var didError: ((Error) -> Void)?
    var didUpdate: ((HomeViewModel) -> Void)?
    var didSelect: ((Topic) -> Void)?
    
    // MARK: - Private
    
    fileprivate let app: Application

    fileprivate(set) var storyViewModels = [StoryCellViewModel]()

    // MARK: - Properties
    
    let storyViewModelsTypes: [StoryCellViewModel.Type] = [StoryCellViewModel.self]

    fileprivate(set) var isUpdating: Bool = false {
        didSet { self.didUpdate?(self) }
    }
    
    // MARK: - Lifeycle
    
    init(_ app: Application) {
        self.app = app
    }

    // MARK: - Actions
    
    func reloadData() {
        self.isUpdating = true
        self.storyViewModels = storiesData.map { self.viewModelFor($0) }
        self.isUpdating = false
    }
    
    // MARK: - Helpers
    
    fileprivate func viewModelFor(_ story: Story) -> StoryCellViewModel {
        let viewModel = StoryCellViewModel(model: story)
        
        viewModel.didSelect = { [weak self] element in
            guard let `self` = self, let topic = element as? Topic else { return }
            self.didSelect?(topic)
        }
        viewModel.didError = { [weak self] error in
            self?.didError?(error)
        }
        return viewModel
    }
}
