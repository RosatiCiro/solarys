//
//  ARViewController.swift
//  Solarys
//
//  Created by Florent Frossard on 18/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import Foundation
import UIKit
import SceneKit
import ARKit

class ARViewController: UIViewController, ARSCNViewDelegate {
    
    @IBOutlet fileprivate var sceneView: ARSCNView!
    
    fileprivate let scene = SCNScene()
    
    fileprivate var planetNode :SCNNode!
    
    fileprivate var planetName: String!
    
    fileprivate var planetSound: String!
    
    fileprivate let radius: CGFloat = 0.3
    
    convenience init(name: String, sound: String) {
        self.init(nibName: nil, bundle: nil)
        self.planetName = name
        self.planetSound = sound
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .black
        navigationController?.setNavigationBarHidden(false, animated: false)
        
        // Set the view's delegate
        sceneView.delegate = self
        // Show statistics such as fps and timing information
        sceneView.showsStatistics = false
        
        let planet = create(self.planetName)
        
        let backgroundMusic = SCNAudioSource(fileNamed: self.planetSound)!
        backgroundMusic.volume = 1
        backgroundMusic.load()
        
        let audioPlayer = SCNAudioPlayer(source: backgroundMusic)
        
        // Set the scene to the view
        sceneView.scene = scene
        scene.background.contents = UIImage(named: "stars_milky_way.jpg")
        sceneView.scene.rootNode.addChildNode(planet)
        planet.addAudioPlayer(audioPlayer)
    }
    
    /// Create the planet by name
    ///
    func create(_ planet: String) -> SCNNode {
        
        let planetSphere = SCNSphere(radius: radius)
        
        let material = SCNMaterial()
        material.diffuse.contents = UIImage(named: "ar\(planet).jpg")
        
        planetSphere.materials = [material]
        
        let planetNode = SCNNode(geometry: planetSphere)
        planetNode.position = SCNVector3(x: 0, y: 0, z: -1)
        
        let rotation = SCNAction.rotateBy(x: 0, y: CGFloat(1), z: 0, duration: TimeInterval(6))
        planetNode.runAction(SCNAction.repeatForever(rotation))
        
        if planet == "Saturn" {
            let saturnLoop = SCNBox(width: 1.5, height: 0, length: 1.5, chamferRadius: 0)
            let ringMaterial = SCNMaterial()
            ringMaterial.diffuse.contents = UIImage(named: "arSaturn_loop.png")
            
            saturnLoop.materials = [ringMaterial]
            
            let loopNode = SCNNode(geometry: saturnLoop)
            loopNode.position = SCNVector3(x: 0, y: 0, z: 0)
            loopNode.rotation = SCNVector4(x: 0.3, y: 0.3, z: 0, w: 1)
            loopNode.scale = SCNVector3(x: 1, y: 1, z: 1)
            
            planetNode.addChildNode(loopNode)
        }
        
        return planetNode
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()
        
        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    // MARK: - ARSCNViewDelegate
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
    }
}

extension ARViewController: NavigationThemeable {
    var navigationBarBackgroundColor: UIColor? { return .clear }
    var navigationBarTintColor: UIColor? { return .white }
}
