//
//  NavigationController.swift
//  Solarys
//
//  Created by Marcello De Palo on 11/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

protocol NavigationThemeable {
    var navigationBarBackgroundColor: UIColor? { get }
    var navigationBarTintColor: UIColor? { get }
}

class NavigationController: UINavigationController {
    
    // MARK: - Properties
    
    var defaultNavBarBackgroundColor: UIColor = .clear
    
    var defaultNavBarTintColor: UIColor = .black
    
    var hideBackButtonText: Bool = true

    // MARK: - Private

    fileprivate var backButtonText = [UIViewController: String?]()

    // MARK: - Lifecycle
    
    required convenience init() {
        self.init(navigationBarClass: nil, toolbarClass: nil)
    }
    
    // MARK: - Navigation
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        self.cacheTitle()
        let root = (self.viewControllers.count == 0)
        self.styleViewController(viewController, root: root)
        super.pushViewController(viewController, animated: animated)
    }
    
    override func popViewController(animated: Bool) -> UIViewController? {
        let destinationVC = self.viewControllers.dropLast().last
        let root = (self.viewControllers.dropLast().count == 1)
        self.uncacheTitle(destinationVC)
        self.styleViewController(destinationVC, root: root)
        return super.popViewController(animated: animated)
    }

    fileprivate func styleViewController(_ vc: UIViewController?, root: Bool) {
        if let vc = vc as? NavigationThemeable {
            let backgroundColor = vc.navigationBarBackgroundColor ?? self.defaultNavBarBackgroundColor
            
            if (root) {
                self.navigationBar.setBackgroundImage(
                    UIImage.fromColor(backgroundColor),
                    for: UIBarMetrics.default
                )
            } else {
                self.navigationBar.barTintColor = backgroundColor
            }
            
            self.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            self.navigationBar.shadowImage = UIImage()

            self.navigationBar.isTranslucent = true
            self.navigationBar.tintColor = vc.navigationBarTintColor ?? self.defaultNavBarTintColor
        }
    }
        
    // MARK: - Back Button
    
    fileprivate func cacheTitle() {
        guard self.hideBackButtonText else { return }
        
        if let vc = self.viewControllers.last {
            self.backButtonText[vc] = vc.title
            vc.title = ""
        }
    }
    fileprivate func uncacheTitle(_ vc: UIViewController?) {
        guard self.hideBackButtonText else { return }
        
        guard
            let vc = vc,
            let cachedTitle = self.backButtonText[vc]
            else { return }

        vc.title = cachedTitle
        self.backButtonText[vc] = nil
    }
}
