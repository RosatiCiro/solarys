//
//  DataLevel.swift
//  Solarys
//
//  Created by Marcello De Palo on 20/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit
import CoreData
import Foundation

@available(*, deprecated, message: "")
struct DataLevel: Codable {
    let story: Int
    let topic: Int
    let level: Int
}

@available(*, deprecated, message: "")
struct DataTopic: Codable {
    let story: Int
    let topic: Int
}
