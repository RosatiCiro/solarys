//
//  Button.swift
//  Solarys
//
//  Created by Marcello De Palo on 21/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import Foundation

struct Button: Decodable {

    private enum CodingKeys: CodingKey {
        case text
        case isEnabled
        case color
        case endpoint
        case maxWidth
    }

    var text: String
    var isEnabled: Bool = true
    var color: String?
    var endpoint: String?
    var maxWidth: Int?

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let text = try container.decode(String.self, forKey: .text)
        
        if let value = try container.decodeIfPresent(Bool.self, forKey: .isEnabled) {
            self.isEnabled = value
        }
        if let value = try container.decodeIfPresent(String.self, forKey: .color) {
            self.color = value
        }
        if let value = try container.decodeIfPresent(String.self, forKey: .endpoint) {
            self.endpoint = value
        }
        if let value = try container.decodeIfPresent(Int.self, forKey: .maxWidth) {
            self.maxWidth = value
        }

        self.text = text
    }
}
 
