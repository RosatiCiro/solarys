//
//  Country.swift
//  Solarys
//
//  Created by Marcello De Palo on 19/12/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import Foundation

struct Country {
        
    static func languageCode() -> String {
        switch Locale.current.languageCode {
        case "it":
            return "it"
        default:
            return "en"
        }
    }
}
