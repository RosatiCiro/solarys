//
//  Answer.swift
//  Solarys
//
//  Created by Marcello De Palo on 11/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

enum AnswerStatus: Int {
    case success
    case selected
    case wrong
    case `default`
    
    var color: UIColor {
        switch self {
        case .success:
            return UIColor.success
        case .wrong:
            return UIColor.danger
        case .selected:
            return UIColor.selected
        default:
            return UIColor.answerBackground
        }
    }
}

class Answer: Decodable {

    enum CodingKeys: CodingKey {
        case id
        case body
        case image
    }

    let id: Int
    let body: String
    var image: String?
    var isSelected = false

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let id = try container.decode(Int.self, forKey: .id)
        let body = try container.decode(String.self, forKey: .body)
        if let value = try container.decodeIfPresent(String.self, forKey: .image) {
            self.image = value
        }

        self.id = id
        self.body = body
    }
}
 
