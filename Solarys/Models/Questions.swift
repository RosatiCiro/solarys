//
//  Questions.swift
//  Solarys
//
//  Created by Marcello De Palo on 11/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import Foundation

struct Questions: Decodable {

    enum CodingKeys: CodingKey {
        case topic
        case level
        case modules
    }

    let topic: Int
    let level: Int
    var modules = [Module]()

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let topic = try container.decode(Int.self, forKey: .topic)
        let level = try container.decode(Int.self, forKey: .level)
        
        if let modules = try container.decodeIfPresent([Module].self, forKey: .modules) {
            self.modules = modules
        }

        self.topic = topic
        self.level = level
    }
}
