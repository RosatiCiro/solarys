//
//  Planet.swift
//  Solarys
//
//  Created by Marcello De Palo on 11/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import Foundation

class Planet: Decodable {

    // MARK: private
    
    fileprivate enum CodingKeys: CodingKey {
        case topic
        case color
        case textColor
        case name
        case image
        case modules
        case offset
        case speed
        case orbit
        case sound
        case story
        case percentage
    }

    fileprivate var levels: [LevelCellViewModel] {
        return modules.compactMap({ element in
            guard
                let cell = element.cellRepresentable,
                let vm = cell as? LevelCellViewModel
                else { return nil }
            return vm
        })
    }

    // MARK: Public

    let name: String
    let color: String
    var textColor: String?
    let image: String
    var modules = [Module]()
    let offset: Double
    let speed: Double
    let orbit: Int
    let sound: String
    var story: Int = 0
    var topic: Int = 0
    var percentage: Double?
    
    var numberOfLevels: Int {
        return levels.count
    }

    var numberOfLevelsCompleted: Int {
        return levels.filter({ $0.isCompleted }).count
    }

    var isComplete: Bool {
        return numberOfLevels == numberOfLevelsCompleted
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let name = try container.decode(String.self, forKey: .name)
        let color = try container.decode(String.self, forKey: .color)
        
        if let modules = try container.decodeIfPresent([Module].self, forKey: .modules) {
            self.modules = modules
        }
        
        if let value = try container.decodeIfPresent(String.self, forKey: .textColor) {
            self.textColor = value
        }

        if let value = try container.decodeIfPresent(Double.self, forKey: .percentage) {
            self.percentage = value
        }
        
        if let value = try container.decodeIfPresent(Int.self, forKey: .story) {
            self.story = value
        }

        if let value = try container.decodeIfPresent(Int.self, forKey: .topic) {
            self.topic = value
        }

        let image = try container.decode(String.self, forKey: .image)
        let offset = try container.decode(Double.self, forKey: .offset)
        let speed = try container.decode(Double.self, forKey: .speed)
        let orbit = try container.decode(Int.self, forKey: .orbit)
        let sound = try container.decode(String.self, forKey: .sound)

        self.name = name
        self.color = color
        self.image = image
        self.offset = offset
        self.speed = speed
        self.orbit = orbit
        self.sound = sound
    }
}
 
