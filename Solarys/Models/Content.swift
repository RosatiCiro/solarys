//
//  Content.swift
//  Solarys
//
//  Created by Marcello De Palo on 21/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import Foundation

struct Content: Decodable {

    enum CodingKeys: CodingKey {
        case modules
        case image
    }

    var modules = [Module]()
    var image: String?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if let modules = try container.decodeIfPresent([Module].self, forKey: .modules) {
            self.modules = modules
        }
        if let value = try container.decodeIfPresent(String.self, forKey: .image) {
            self.image = value
        }
    }
}
 
