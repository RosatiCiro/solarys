//
//  Module.swift
//  Solarys
//
//  Created by Marcello De Palo on 11/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import Foundation

enum Module {
    case answer(AnswerCellViewModel)
    case button(ButtonCellViewModel)
    case content(ContentCellViewModel)
    case level(LevelCellViewModel)
    case planet(PlanetCellViewModel)
    case space(SpaceCellViewModel)
    case question(QuestionCellViewModel)
    case questions(QuestionsCellViewModel)
    case topic(TopicCellViewModel)
    case title(TitleCellViewModel)
    case unblocker(UnblockerCellViewModel)
    case unsupported
}

extension Module {
    var cellRepresentable: CellRepresentable? {
        let isCellRepresentable = { (module: AnyObject) -> CellRepresentable? in
            guard let cell = module as? CellRepresentable else { return nil }
            return cell
        }
        
        switch self {
        case .answer(let cell):
            return isCellRepresentable(cell)
        case .button(let cell):
            return isCellRepresentable(cell)
        case .topic(let cell):
            return isCellRepresentable(cell)
        case .content(let cell):
            return isCellRepresentable(cell)
        case .level(let cell):
            return isCellRepresentable(cell)
        case .question(let cell):
            return isCellRepresentable(cell)
        case .questions(let cell):
            return isCellRepresentable(cell)
        case .planet(let cell):
            return isCellRepresentable(cell)
        case .space(let cell):
            return isCellRepresentable(cell)
        case .title(let cell):
            return isCellRepresentable(cell)
        case .unblocker(let cell):
            return isCellRepresentable(cell)
        default:
            return nil
        }
    }
    
    var cellType: CellRepresentable.Type? {
        guard let cell = self.cellRepresentable else { return nil }
        return cell.type
    }
    
    var viewModel: ViewModel? {
        guard let cell = self.cellRepresentable else { return nil }
        guard let vm = cell as? ViewModel else { return nil }
        return vm
    }
    
    var id: UUID {
        guard let vm = self as? ViewModel else { return UUID() }
        return vm.id
    }
}

extension Module {
    
    func `as`<T: ViewModel>(_ type: T.Type) -> T? {
        guard let vm = self.viewModel as? T else {
            return nil
        }
        return vm
    }
}

// MARK: - Decodable

extension Module: Decodable {
    
    private enum CodingKeys: CodingKey {
        case type
    }
        
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        // get module type
        let type = try container.decode(String.self, forKey: .type)
        
        switch type {
             case "answer":
                let vm = try AnswerCellViewModel(from: decoder)
                self = .answer(vm)
             case "button":
                let vm = try ButtonCellViewModel(from: decoder)
                self = .button(vm)
            case "topic":
                let vm = try TopicCellViewModel(from: decoder)
                self = .topic(vm)
             case "content":
                let vm = try ContentCellViewModel(from: decoder)
                self = .content(vm)
            case "level":
               let vm = try LevelCellViewModel(from: decoder)
               self = .level(vm)
             case "question":
                let vm = try QuestionCellViewModel(from: decoder)
                self = .question(vm)
             case "questions":
                let vm = try QuestionsCellViewModel(from: decoder)
                self = .questions(vm)
            case "planet":
                let vm = try PlanetCellViewModel(from: decoder)
                self = .planet(vm)
            case "space":
                let vm = try SpaceCellViewModel(from: decoder)
                self = .space(vm)
            case "title":
                let vm = try TitleCellViewModel(from: decoder)
                self = .title(vm)
            case "unblocker":
                let vm = try UnblockerCellViewModel(from: decoder)
                self = .unblocker(vm)
            default:
                self = .unsupported
        }
    }
}

// MARK: - Equatable

extension Module: Equatable {
    static func == (lhs: Module, rhs: Module) -> Bool {
        return (lhs.id == rhs.id)
    }
}
