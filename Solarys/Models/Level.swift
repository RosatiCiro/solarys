//
//  Level.swift
//  Solarys
//
//  Created by Marcello De Palo on 11/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import Foundation
import CoreData
import UIKit

struct Level: Decodable {

    enum CodingKeys: CodingKey {
        case story
        case topic
        case level
        case modules
    }

    let story: Int
    let topic: Int
    let level: Int
    var modules = [Module]()
    var isComplete: Bool = false
    var percentage: Double = 0 {
        didSet {
            isComplete = percentage == 1
        }
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let story = try container.decode(Int.self, forKey: .story)
        let topic = try container.decode(Int.self, forKey: .topic)
        let level = try container.decode(Int.self, forKey: .level)
        
        if let modules = try container.decodeIfPresent([Module].self, forKey: .modules) {
            self.modules = modules
        }

        self.story = story
        self.topic = topic
        self.level = level
    }
}
