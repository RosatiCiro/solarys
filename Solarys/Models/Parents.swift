//
//  Parents.swift
//  Solarys
//
//  Created by Marcello De Palo on 16/01/2020.
//  Copyright © 2020 Marcello De Palo. All rights reserved.
//

import Foundation

struct Parents: Codable {
    let story: Int
    let topics: [Int]
}
