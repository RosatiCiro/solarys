//
//  Story.swift
//  Solarys
//
//  Created by Marcello De Palo on 02/12/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import Foundation

class Story: Decodable {

    // MARK: private
    
    fileprivate enum CodingKeys: CodingKey {
        case height
        case layout
        case modules
        case title
        case scrollToItem
    }

    // MARK: Public

    var height: Double?
    var layout: String?
    var modules = [Module]()
    var scrollToItem: Int = 0
    let title: String

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.title = try container.decode(String.self, forKey: .title)

        if let value = try container.decodeIfPresent(String.self, forKey: .layout) {
            self.layout = value
        }

        if let value = try container.decodeIfPresent(Double.self, forKey: .height) {
            self.height = value
        }

        if let value = try container.decodeIfPresent(Int.self, forKey: .scrollToItem) {
            self.scrollToItem = value
        }

        if let modules = try container.decodeIfPresent([Module].self, forKey: .modules) {
            self.modules = modules
        }
    }
}
