//
//  DataManager.swift
//  Solarys
//
//  Created by Marcello De Palo on 20/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit
import CoreData

class DataManager {
    
    static let shared = DataManager()

    // MARK: - Private

    private var context: NSManagedObjectContext

    // MARK: - Lyfe cicle

    private init() {
        let application = UIApplication.shared.delegate as! AppDelegate
        self.context = application.persistentContainer.viewContext
    }
    
    // MARK: - API

    /// Save data by Dictionary
    /// - Parameters:
    ///   - details: The data to store
    ///   - entityName: The name of the Entity
    func store(_ details: Dictionary<String, AnyObject>, for entityName: String) {
        let entity = NSEntityDescription.entity(forEntityName: entityName, in: context)
        let managedObject = NSManagedObject(entity: entity!, insertInto: context)
        
        for (key, value) in details {
            managedObject.setValue(value, forKey: key)
        }
        
        do {
            try context.save()
        } catch { }
    }
    
    /// Delete all record for Entity
    /// - Parameter entityName: The name of thhe entity
    ///
    func reset(_ entityName: String) {
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        let request = NSBatchDeleteRequest(fetchRequest: fetch)

        do {
            try context.execute(request)
        } catch { }
    }
    
    /// Fetch all data History
    ///
    func fetchHistory() -> [History] {
        let fetchRequest: NSFetchRequest<History> = History.fetchRequest()
        do {
            return try self.context.fetch(fetchRequest)
        } catch {
            return [History]()
        }
    }
    
    /// Fetch History from params
    /// - Parameters:
    ///   - story: Int
    ///   - topic: Int
    ///   - level: Int
    ///
    func fetchHistory(story: Int, topic: Int, level: Int) -> [History] {
        let fetchRequest: NSFetchRequest<History> = History.fetchRequest()
        fetchRequest.returnsObjectsAsFaults = false
        
        let predicates = [
            NSPredicate(format: "story == \(Int16(story))"),
            NSPredicate(format: "topic == \(Int16(topic))"),
            NSPredicate(format: "level == \(Int16(level))")
        ]
        
        let predicate = NSCompoundPredicate(type: .and, subpredicates: predicates)
        fetchRequest.predicate = predicate
        
        return historyFromFetchRequest(fetchRequest)
    }
    
    /// Delete History
    /// - Parameters:
    ///   - story: Int
    ///   - topic: Int
    ///   - level: Int
    ///
    func deleteHistory(story: Int, topic: Int, level: Int) {
        do {
            let levels = fetchHistory(story: story, topic: topic, level: level)
            for level in levels {
                context.delete(level)
            }
            try context.save()
        } catch { }
    }
    
    /// Fetch History by NSFetchRequest
    /// - Parameter request: NSFetchRequest
    ///
    private func historyFromFetchRequest(_ request: NSFetchRequest<History>) -> [History] {
        do {
            return try self.context.fetch(request)
        } catch {
            return [History]()
        }
    }
    
    // MARK: - UserDefaults

    @available(*, deprecated, message: "")
    static func loadValue<T: Decodable>(for typeName: String, decodingType: T.Type) -> T? {
        guard
            let jsonString = UserDefaults.standard.object(forKey: "solaris.\(typeName)") as? String,
            let jsonData = jsonString.data(using: String.Encoding.utf8),
            let value = try? JSONDecoder().decode(decodingType, from: jsonData)
            else {
                return nil
        }
        return value
    }
    
    @available(*, deprecated, message: "")
    static func reset() {
        UserDefaults.standard.removeObject(forKey: "solaris.history")
        UserDefaults.standard.removeObject(forKey: "solaris.levels")
    }
}
