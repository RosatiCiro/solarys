//
//  DataHistory.swift
//  Solarys
//
//  Created by Marcello De Palo on 20/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import Foundation

@available(*, deprecated, message: "")
class DataHistory<Element: Encodable> {
    
    var items = [Element]()

    // MARK: - Life Cycle

    init() { }

    init(_ element: Element) {
        self.push(element)
    }
    
    init(_ elements: [Element]) {
        self.items = elements
    }

    func push(_ item: Element) {
        self.items.append(item)
    }
    
    /// Fetch all `items`in the History
    ///
    func fetch() -> [Element] {
        return self.items
    }

    /// Fetch `items` in the History by `topic`
    ///
    func fetchBy(_ story: Int, topic: Int) -> [Element] {
        return items.filter({ item in
            if let level = item as? DataLevel {
                return level.story == story && level.topic == topic
            }
            return false
        })
    }
    
    /// Fetch `items` in the History by topic` and level
    ///
    func fetchBy(_ story: Int, topic: Int, level: Int) -> [Element] {
        return items.filter({ item in
            if let userLevel = item as? DataLevel {
                return userLevel.story == story &&
                    userLevel.topic == topic &&
                    userLevel.level == level
            }
            return false
        })
    }
    
    func delete(_ story: Int, topic: Int, level: Int) {
        self.items = items.filter({ item in
            if let userLevel = item as? DataLevel {
                return userLevel.story != story &&
                    userLevel.topic != topic &&
                    userLevel.level != level
            }
            return false
        })
    }
}
