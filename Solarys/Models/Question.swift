//
//  Question.swift
//  Solarys
//
//  Created by Marcello De Palo on 11/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import Foundation

class Question: Decodable {

    enum CodingKeys: CodingKey {
        case id
        case answer
        case modules
        case numberOfColumns
        case text
    }

    let id: Int
    let correctAnswerNumber: Int
    var correctAnswerText: String
    var modules = [Module]()
    let numberOfColumns: Int
    var isCompleted: Bool = false

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let id = try container.decode(Int.self, forKey: .id)
        let answer = try container.decode(Int.self, forKey: .answer)
        let text = try container.decode(String.self, forKey: .text)

        if let modules = try container.decodeIfPresent([Module].self, forKey: .modules) {
            self.modules = modules
        }

        let numberOfColumns = try container.decode(Int.self, forKey: .numberOfColumns)

        self.id = id
        self.correctAnswerNumber = answer
        self.correctAnswerText = text
        self.numberOfColumns = numberOfColumns
    }
}

