//
//  Topic.swift
//  Solarys
//
//  Created by Marcello De Palo on 12/01/2020.
//  Copyright © 2020 Marcello De Palo. All rights reserved.
//

import Foundation

class Topic: Decodable {

    // MARK: private
    
    fileprivate enum CodingKeys: CodingKey {
        case topic
        case color
        case textColor
        case name
        case image
        case modules
        case sound
        case story
        case segment
        case percentage
        case parents
    }

    fileprivate var levels: [LevelCellViewModel] {
        return modules.compactMap({ element in
            guard
                let cell = element.cellRepresentable,
                let vm = cell as? LevelCellViewModel
                else { return nil }
            return vm
        })
    }

    // MARK: Public

    let name: String
    let color: String
    var textColor: String?
    var segment: String
    let image: String
    var modules = [Module]()
    let sound: String
    var story: Int = 0
    var topic: Int = 0
    var percentage: Double?
    var parents: Parents?
    var isLocked: Bool {
        return isTopicLoked()
    }
    
    var numberOfLevels: Int {
        return levels.count
    }

    var numberOfLevelsCompleted: Int {
        return levels.filter({ $0.isCompleted }).count
    }

    var isComplete: Bool {
        return numberOfLevels == numberOfLevelsCompleted
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let name = try container.decode(String.self, forKey: .name)
        let color = try container.decode(String.self, forKey: .color)
        
        if let modules = try container.decodeIfPresent([Module].self, forKey: .modules) {
            self.modules = modules
        }
        
        if let value = try container.decodeIfPresent(String.self, forKey: .textColor) {
            self.textColor = value
        }

        if let value = try container.decodeIfPresent(Double.self, forKey: .percentage) {
            self.percentage = value
        }
        
        if let value = try container.decodeIfPresent(Int.self, forKey: .story) {
            self.story = value
        }

        if let value = try container.decodeIfPresent(Int.self, forKey: .topic) {
            self.topic = value
        }
        
        if let value = try container.decodeIfPresent(Parents.self, forKey: .parents) {
            self.parents = value
        }
        
        let image = try container.decode(String.self, forKey: .image)
        let sound = try container.decode(String.self, forKey: .sound)
        let segment = try container.decode(String.self, forKey: .segment)

        self.name = name
        self.color = color
        self.image = image
        self.sound = sound
        self.segment = segment
    }
    
    /// Return bool value if topic is loked or not
    ///
    func isTopicLoked() -> Bool {
        guard let parents = self.parents else { return false }
        
        let completed = storiesData
            .flatMap { $0.modules.filter({
                    element in
                    if let topic = element.as(TopicCellViewModel.self) {
                        return topic.story == parents.story && topic.isComplete
                    }
                    return false
                })
            }
            .reduce(into: [Int]()) {
                (completed, element) in
                guard let vm = element.as(TopicCellViewModel.self) else { return }
                completed.append(vm.topic)
            }

        let listCompleted = Set(completed)
        let findCompleted = Set<Int>(parents.topics)
        return !findCompleted.isSubset(of: listCompleted)
    }
}
 
