//
//  TitleCell.swift
//  Solarys
//
//  Created by Marcello De Palo on 02/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

class TitleCell: UICollectionViewCell, NibLoadableView {
    
    // MARK: - IBOutlets

    @IBOutlet private weak var titleLabel: UILabel!

    @IBOutlet fileprivate weak var widthLayoutConstraint: NSLayoutConstraint!

    // MARK: - Initialization
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        widthLayoutConstraint.constant = bounds.inset(by: layoutMargins).width
    }

    // MARK: - mvvm
    
    func setup(_ viewModel: TitleCellViewModel) {
        titleLabel.text = viewModel.text
        titleLabel.textAlignment = viewModel.alignement.type
        titleLabel.font = .for(style: .h3)

        if let style = viewModel.style, let uiStyle = UIFont.Style(rawValue: style) {
            titleLabel.font = UIFont.for(style: uiStyle)
        }
    }
    
    // MARK: - Layout

    override func layoutSubviews() {
        super.layoutSubviews()
        widthLayoutConstraint.constant = bounds.inset(by: layoutMargins).width
    }

    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
        layoutIfNeeded()
    }
}
