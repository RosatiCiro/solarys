//
//  TitleCellViewModel.swift
//  Solarys
//
//  Created by Marcello De Palo on 02/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

class TitleCellViewModel: ViewModel {

    // MARK: - Private

    private enum CodingKeys: CodingKey {
        case text
        case alignement
        case style
    }
    
    enum Alignement: String, CaseIterable, Codable, Hashable  {
        case left = "left"
        case center = "center"
        case right = "right"
        
        var type: NSTextAlignment {
            switch self {
            case .left: return .left
            case .center: return .center
            case .right: return .right
            }
        }
    }

    // MARK: - Properties

    var type: CellRepresentable.Type = TitleCellViewModel.self

    let text: String?

    var alignement: Alignement = .left

    var style: String?

    // MARK: - Events
    
    var didError: ((Error) -> Void)?
    var didSelect: ((AnyObject?) -> Void)?
    var didUpdate: ((Any) -> Void)?

    // MARK: - Lifecycle
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        text = try container.decodeIfPresent(String.self, forKey: .text)

        if let value = try container.decodeIfPresent(String.self, forKey: .alignement) {
            self.alignement = (Alignement(rawValue: value) ?? .left)
        }
        if let value = try container.decodeIfPresent(String.self, forKey: .style) {
            self.style = value
        }
    }

    deinit { }
}

// MARK: - CellRepresentable

extension TitleCellViewModel: CellRepresentable {
    

    static func registerCell(_ collectionView: UICollectionView) {
        collectionView.register(TitleCell.self)
    }
    
    func dequeueCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell? {
        let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as TitleCell
        cell.setup(self)
        return cell
    }

    func selected() { /* */ }
    
    func size() -> CGSize {
        return size(forItemIn: .zero)
    }
    
    func size(forItemIn rect: CGRect) -> CGSize {
        if let cell = UIView.loadFromNibNamed(nibNamed: "TitleCell") as? TitleCell {

            cell.prepareForReuse()
    
            cell.setup(self)
    
            cell.setNeedsUpdateConstraints()
            cell.updateConstraintsIfNeeded()
    
            cell.bounds = CGRect(x: 0.0, y: 0.0, width: rect.width, height: cell.bounds.height);
    
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
    
            let height = cell.contentView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
            return CGSize(width: rect.width, height: height)
        }

        return CGSize(width: rect.width, height: 50)
    }
}
