//
//  QuestionsCell.swift
//  Solarys
//
//  Created by Marcello De Palo on 11/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

class QuestionsCell: UICollectionViewCell, NibLoadableView {
    
    // MARK: - IBOutlets

    @IBOutlet fileprivate weak var collectionView: UICollectionView!

    // MARK: - Private

    fileprivate weak var delegate: QuestionsCellViewModel!

    // MARK: - mvvm
    
    func setup(_ viewModel: QuestionsCellViewModel) {
        delegate = viewModel
        
        reset()

        viewModel.viewModelsTypes.forEach { $0.registerCell(self.collectionView) }

        viewModel.didUpdateToNext = didUpdateToNext

        collectionView.delegate = viewModel
        collectionView.dataSource = viewModel
    }
    
    /// Reset data for restart the current questions
    ///
    fileprivate func reset() {
        delegate.currentQuestion = 0
    }

    /// Move to next question/content
    ///
    fileprivate func didUpdateToNext(index: Int) {
        collectionView.scrollToItem(at: IndexPath(item: index, section: 0),
                                    at: .centeredHorizontally,
                                    animated: true)
    }
    
    // MARK: - Layout

    override func layoutSubviews() {
        super.layoutSubviews()
        collectionView.collectionViewLayout.invalidateLayout()
    }
    
    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
        layoutIfNeeded()
    }
}
