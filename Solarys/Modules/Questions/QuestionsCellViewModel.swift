//
//  QuestionsCellViewModel.swift
//  Solarys
//
//  Created by Marcello De Palo on 11/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

class QuestionsCellViewModel: NSObject, ViewModel {

    // MARK: - Private

    fileprivate enum CodingKeys: CodingKey {
        case title
        case subtitle
    }

    fileprivate var model: Questions!
    
    fileprivate(set) var viewModelsTypes: [CellRepresentable.Type] = []
    
    fileprivate var viewModels = [CellRepresentable]()

    fileprivate var questions = [QuestionCellViewModel]()
    
    internal var currentQuestion = 0

    // MARK: - Properties

    var type: CellRepresentable.Type = QuestionsCellViewModel.self
      
    var isCompleted: Bool {
        return numberOfQuestions == numberOfCompleted
    }
    
    var numberOfQuestions: Int {
        return questions.count
    }

    var numberOfCompleted: Int {
        return questions.filter({ $0.isCompleted }).count
    }

    var percentage: Double {
        return Double((100 / self.numberOfQuestions) * self.numberOfCompleted) / 100
    }

    // MARK: - Events
    
    var didError: ((Error) -> Void)?
    var didSelect: ((AnyObject?) -> Void)?
    var didUpdate: ((Any) -> Void)?
    var didUpdateToNext: ((Int) -> Void)?

    // MARK: - Lifecycle
    
    required init(model: Any) {
        super.init()
        guard let model = model as? Questions else { return }
        self.model = model
        
        reloadData()
    }
    
    required init(from decoder: Decoder) throws {
        super.init()
        self.model = try Questions(from: decoder)
        
        reloadData()
    }

    deinit { }
    
    fileprivate func reloadData() {
        viewModels = model.modules.compactMap { self.viewModelFor($0) }
        
        questions = (viewModels.filter({
            $0.type == QuestionCellViewModel.self
        }) as? [QuestionCellViewModel]) ?? []
    }
    
    fileprivate func viewModelFor(_ module: Module) -> CellRepresentable? {
        guard
            let cell = module.cellRepresentable,
            var viewModel = cell as? ViewModel
            else { return nil }
        
        viewModel.didUpdate = { [weak self] element in
            guard let `self` = self else { return }
            self.didUpdate?(self)
        }

        viewModelsTypes.append(cell.type)
        return cell
    }
}

// MARK: - CellRepresentable

extension QuestionsCellViewModel: CellRepresentable {
    
    static func registerCell(_ collectionView: UICollectionView) {
        collectionView.register(QuestionsCell.self)
    }
    
    func dequeueCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell? {
        let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as QuestionsCell
        cell.setup(self)
        return cell
    }

    func selected() { /* */ }
    
    func size() -> CGSize {
        return size(forItemIn: .zero)
    }
    
    func size(forItemIn rect: CGRect) -> CGSize {
        return rect.size
    }
}

// MARK: - UICollectionViewDataSource

extension QuestionsCellViewModel: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = viewModels[indexPath.item].dequeueCell(collectionView, cellForItemAt: indexPath)
        return cell ?? UICollectionViewCell()
    }
}

// MARK: - UICollectionViewDelegate

extension QuestionsCellViewModel : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        // if the user fail to the question move the current to the queue
        if let vm = viewModels[indexPath.item] as? QuestionCellViewModel {
            currentQuestion += 1

            if !vm.isCompleted {
                // --- WRONG question
                let newIndex = viewModels.count
                vm.reset()
                viewModels.append(vm)
                collectionView.performBatchUpdates({
                    collectionView.insertItems(at: [IndexPath(item: newIndex, section: 0)])
                }, completion: { _ in
                    self.didUpdateToNext?(self.currentQuestion)
                })
            } else if isCompleted {
                // --- ALL questions complete
                //self.didUpdate?(ApplicationUpdate.levelDone)
                // ------------------
                // schermata intermedia da JSON content
                self.didUpdate?(self.percentage)
                // ------------------
            } else {
                // --- NEXT question
                self.didUpdateToNext?(self.currentQuestion)
            }
        } else {
        }
    }
}

// MARK: - UICollectionViewDelegateFlowLayout

extension QuestionsCellViewModel : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return viewModels[indexPath.item].size(forItemIn: collectionView.frame)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        targetContentOffsetForProposedContentOffset proposedContentOffset: CGPoint) -> CGPoint {
        let x = CGFloat(currentQuestion) * collectionView.bounds.size.width
        return CGPoint(x: x,
                       y: proposedContentOffset.y)
    }
}
