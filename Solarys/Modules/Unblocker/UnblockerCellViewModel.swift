//
//  UnblockerCellViewModel.swift
//  Solarys
//
//  Created by Marcello De Palo on 02/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

class UnblockerCellViewModel: ViewModel {

    // MARK: - Private

    private enum CodingKeys: CodingKey {
        case title
        case isEnabled
        case color
    }
    
    // MARK: - Properties

    var type: CellRepresentable.Type = UnblockerCellViewModel.self

    var title: String

    // MARK: - Events
    
    var didError: ((Error) -> Void)?
    var didSelect: ((AnyObject?) -> Void)?
    var didUpdate: ((Any) -> Void)?

    // MARK: - Lifecycle
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.title = try container.decode(String.self, forKey: .title)
    }

    deinit { }
}

// MARK: - CellRepresentable

extension UnblockerCellViewModel: CellRepresentable {
    
    static func registerCell(_ collectionView: UICollectionView) {
        collectionView.register(UnblockerCell.self)
    }
    
    func dequeueCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell? {
        let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as UnblockerCell
        cell.setup(self)
        return cell
    }

    func selected() {
    }
    
    func size() -> CGSize {
        return size(forItemIn: .zero)
    }
    
    func size(forItemIn rect: CGRect) -> CGSize {
        return CGSize(width: rect.width, height: 110)
    }
}
