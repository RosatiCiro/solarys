//
//  UnblockerCell.swift
//  Solarys
//
//  Created by Marcello De Palo on 02/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

class UnblockerCell: UICollectionViewCell, NibLoadableView {
    
    // MARK: - IBOutlets

    @IBOutlet private weak var titleLabel: UILabel!

    @IBOutlet private weak var lineLeftView: UIView!

    @IBOutlet private weak var lineRightView: UIView!

    // MARK: private
    
    fileprivate var delegate: UnblockerCellViewModel!
    
    // MARK: - Initialization
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }

    // MARK: - mvvm
    
    func setup(_ viewModel: UnblockerCellViewModel) {
        
        titleLabel.text = viewModel.title
        titleLabel.font = UIFont.for(style: .h1)
        
        lineLeftView.layer.cornerRadius = lineLeftView.frame.height / 2
        lineRightView.layer.cornerRadius = lineRightView.frame.height / 2
    }
    
    // MARK: - Layout

    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
        layoutIfNeeded()
    }
}
