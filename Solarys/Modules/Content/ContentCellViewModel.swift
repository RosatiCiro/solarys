//
//  ContentCellViewModel.swift
//  Solarys
//
//  Created by Marcello De Palo on 02/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

class ContentCellViewModel: NSObject, ViewModel {

    // MARK: - Private

    fileprivate var model: Content!

    fileprivate(set) var viewModelsTypes: [CellRepresentable.Type] = []
    
    fileprivate var viewModels = [CellRepresentable]()

    // MARK: - Properties

    var type: CellRepresentable.Type = ContentCellViewModel.self

    var image: String? { return model.image }
    
    // MARK: - Events
    
    var didError: ((Error) -> Void)?
    var didSelect: ((AnyObject?) -> Void)?
    var didUpdate: ((Any) -> Void)?

    // MARK: - Lifecycle
    
    required init(model: Any) {
        super.init()
        guard let model = model as? Content else { return }
        self.model = model
        
        configure()
    }
    
    required init(from decoder: Decoder) throws {
        super.init()
        self.model = try Content(from: decoder)
        
        configure()
    }

    deinit { }

    fileprivate func configure() {
        viewModels = model.modules.compactMap { self.viewModelFor($0) }
    }

    fileprivate func viewModelFor(_ module: Module) -> CellRepresentable? {
        guard
            let cellRepresentable = module.cellRepresentable,
            var viewModel = cellRepresentable as? ViewModel
            else { return nil }
        
        viewModel.didSelect = { [weak self] element in
            guard let `self` = self else { return }
            self.didSelect?(element)
        }

        viewModel.didUpdate = { [weak self] element in
            guard let `self` = self else { return }
            self.didUpdate?(element)
        }

        viewModelsTypes.append(cellRepresentable.type)

        return cellRepresentable
    }
}

// MARK: - CellRepresentable

extension ContentCellViewModel: CellRepresentable {
    
    static func registerCell(_ collectionView: UICollectionView) {
        collectionView.register(ContentCell.self)
    }
    
    func dequeueCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell? {
        let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as ContentCell
        cell.setup(self)
        return cell
    }

    func selected() { /* */ }
    
    func size() -> CGSize {
        return size(forItemIn: .zero)
    }
    
    func size(forItemIn rect: CGRect) -> CGSize {
        return rect.size
    }
}

// MARK: - UICollectionViewDataSource

extension ContentCellViewModel: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = viewModels[indexPath.item].dequeueCell(collectionView, cellForItemAt: indexPath)
        return cell ?? UICollectionViewCell()
    }
}

// MARK: - UICollectionViewDelegate

extension ContentCellViewModel: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModels[indexPath.item].selected()
    }
}

// MARK: - UICollectionViewDelegateFlowLayout

extension ContentCellViewModel: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return viewModels[indexPath.item].size(forItemIn: collectionView.frame)
    }
}

// MARK: - UICollectionViewDataSource

extension ContentCellViewModel: LayoutDelegate {
    
    func collectionView(_ collectionView: UICollectionView, sizeForItemAt indexPath: IndexPath, withWidth: CGFloat) -> CGSize {
        return self.collectionView(collectionView, layout: collectionView.collectionViewLayout, sizeForItemAt: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewModelForItemAt indexPath: IndexPath) -> CellRepresentable {
        return viewModels[indexPath.item]
    }
}
