//
//  ContentLayout.swift
//  Solarys
//
//  Created by Marcello De Palo on 21/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

class ContentLayout: UICollectionViewFlowLayout {
    
    // MARK: - Properties
    
    struct SectionLimit {
        let top: CGFloat
        let bottom: CGFloat
        let type: CellRepresentable.Type
    }
    
    var delegate: LayoutDelegate!

    var currentAttributes = [UICollectionViewLayoutAttributes]()
        
    private var currentContentSize: CGSize = .zero
    
    private var sectionLimits: [SectionLimit] = []
    
    private var availableWidth: CGFloat = UIScreen.main.bounds.width
    
    private var availableHeight: CGFloat = UIScreen.main.bounds.height
    
    private var cellFrames = [CGRect]()
    
    private var contentHeight: CGFloat = 0.0
    
    private var contentInset: UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
    }
    
    func prepareContentSize(_ collectionView: UICollectionView) {
        
        let rect = collectionView.frame
        
        availableWidth = rect.width - contentInset.left - contentInset.right
        
        availableHeight = rect.height
        
        cellFrames = [CGRect]()
        
        var y: CGFloat = contentInset.top
        
        let itemCount = collectionView.numberOfItems(inSection: 0)

        for index in 0..<itemCount {
            let sectionTop = y
            
            let indexPath = IndexPath(item: index, section: 0)

            let viewModel = delegate.collectionView(collectionView, viewModelForItemAt: indexPath)
            
            let cellWidth = availableWidth
            let cellHeight = delegate.collectionView(collectionView,
                                                     sizeForItemAt: indexPath,
                                                     withWidth: rect.width).height
            let frame = CGRect(x: contentInset.left, y: y, width: cellWidth, height: cellHeight)
            
            y += frame.height
            
            let sectionBottom = y
            
            cellFrames.append(frame)
            
            sectionLimits.append(SectionLimit(top: sectionTop,
                                              bottom: sectionBottom,
                                              type: viewModel.type))
        }
    }
    
    override func prepare() {
        guard let collectionView = collectionView else { return }
        
        prepareContentSize(collectionView)
                
        currentContentSize = CGSize.zero
        
        currentAttributes = []
        
        let availableWidth = collectionView.bounds.inset(by: collectionView.layoutMargins).width
        let availableHeight = collectionView.bounds.inset(by: collectionView.layoutMargins).height
        
        let itemCount = collectionView.numberOfItems(inSection: 0)
        
        for itemIndex in 0..<itemCount {
            let indexPath = IndexPath(item: itemIndex, section: 0)
            let viewModel = delegate.collectionView(collectionView, viewModelForItemAt: indexPath)
            let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
            
            var frame = cellFrames[itemIndex]
            
            if let _ = viewModel as? ButtonCellViewModel {
                let frameMaxY = availableHeight - frame.height
                frame.origin.y = frameMaxY
            }
            
            if let _ = viewModel as? PlanetCellViewModel {
                let sectionLimitTop = sectionLimits.last?.bottom ?? 0.0
                let frameY: CGFloat = ((availableHeight - sectionLimitTop) / 2)
                frame.origin.y += frameY
            }
            
            attributes.frame = frame
            
            currentAttributes.append(attributes)
        }
        
        currentContentSize = CGSize(width: availableWidth, height: availableHeight)
    }
    
    // MARK: - Layout Attributes
        
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        return currentAttributes.filter { rect.intersects($0.frame) }
    }
    
    // MARK: - Invalidation
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        if let oldBounds = collectionView?.bounds, !oldBounds.size.equalTo(newBounds.size) {
            return true
        }
        
        return false
    }
    
    // MARK: - Collection View Info
    
    override var collectionViewContentSize : CGSize {
        return currentContentSize
    }
}
