//
//  ContentCell.swift
//  Solarys
//
//  Created by Marcello De Palo on 02/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

class ContentCell: UICollectionViewCell, NibLoadableView {
    
    // MARK: - IBOutlets

    @IBOutlet fileprivate weak var collectionView: UICollectionView!

    @IBOutlet fileprivate weak var imageView: UIImageView!

    // MARK: - Initialization
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: - mvvm

    func setup(_ viewModel: ContentCellViewModel) {
        
        viewModel.viewModelsTypes.forEach { $0.registerCell(self.collectionView) }
        
        if let image = viewModel.image, let uiImage = UIImage(named: image) {
            imageView.image = uiImage
        }
        
        if let layout = collectionView.collectionViewLayout as? ContentLayout {
          layout.delegate = viewModel
        }

        collectionView.delegate = viewModel
        collectionView.dataSource = viewModel
    }
    
    // MARK: - Layout

    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
        layoutIfNeeded()
    }
}
