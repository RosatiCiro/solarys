//
//  TitleCell.swift
//  Solarys
//
//  Created by Marcello De Palo on 02/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

class SpaceCell: UICollectionViewCell, NibLoadableView {
    
    // MARK: - Initialization
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: - mvvm
    
    func setup(_ viewModel: SpaceCellViewModel) {
    }
}
