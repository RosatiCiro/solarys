//
//  TitleCellViewModel.swift
//  Solarys
//
//  Created by Marcello De Palo on 02/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

class SpaceCellViewModel: ViewModel {
    
    // MARK: - Private
    
    private enum CodingKeys: CodingKey {
        case height
    }
    
    // MARK: - Properties
    
    var type: CellRepresentable.Type = SpaceCellViewModel.self
    
    //    let text: String?
    //
    var height: CGFloat = 0
    
    // MARK: - Lifecycle
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        if let value = try container.decodeIfPresent(Int.self, forKey: .height) {
            self.height = CGFloat(value)
        }
    }
    
    deinit { }
    
    // MARK: - Events
    
    var didError: ((Error) -> Void)?
    var didSelect: ((AnyObject?) -> Void)?
    var didUpdate: ((Any) -> Void)?
}

// MARK: - CellRepresentable

extension SpaceCellViewModel: CellRepresentable {
    
    static func registerCell(_ collectionView: UICollectionView) {
        collectionView.register(SpaceCell.self)
    }
    
    func dequeueCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell? {
        let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as SpaceCell
        cell.setup(self)
        return cell
    }
    
    func selected() { /* */ }
    
    func size() -> CGSize {
        return size(forItemIn: .zero)
    }
    
    func size(forItemIn rect: CGRect) -> CGSize {
        return CGSize(width: rect.width, height: self.height)
    }
}
