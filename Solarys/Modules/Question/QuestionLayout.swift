//
//  QuestionLayout.swift
//  Solarys
//
//  Created by Marcello De Palo on 14/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

class QuestionLayout: UICollectionViewFlowLayout {
    
    // MARK: - Properties

    struct SectionLimit {
        let top: CGFloat
        let bottom: CGFloat
        let type: CellRepresentable.Type
    }

    var delegate: QuestionCellViewModel!
        
    var currentAttributes = [UICollectionViewLayoutAttributes]()
    
    var previousAttributes: [UICollectionViewLayoutAttributes] = []

    private var currentContentSize = CGSize.zero

    private var sectionLimits: [SectionLimit] = []

    private var availableWidth: CGFloat = UIScreen.main.bounds.width
    
    private var availableHeight: CGFloat = UIScreen.main.bounds.height

    private var cellFrames = [CGRect]()

    private var contentHeight: CGFloat = 0.0

    private var contentInset: UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 10, bottom: 0, right: 10)
    }

    func prepareContentSize(_ collectionView: UICollectionView) {

        let rect = collectionView.frame
 
        availableWidth = rect.width - contentInset.left - contentInset.right
        
        availableHeight = rect.height

        cellFrames = [CGRect]()
        
        sectionLimits = [SectionLimit]()
                
        var y: CGFloat = contentInset.top
        
        let numberOfColumns = delegate.numberOfColumns
        
        var xOffset = [CGFloat]()
        for column in 0 ..< numberOfColumns {
            xOffset.append((CGFloat(column) * (availableWidth / CGFloat(numberOfColumns))))
        }

        var yOffset = [CGFloat](repeating: 0, count: numberOfColumns)

        var column = 0

        for index in 0..<delegate.numberOfItems {
            let viewModel = delegate.viewModel(for: index)
            let isAnswer = viewModel.type == AnswerCellViewModel.self
            let sectionTop = y
            
            var columnWidth = availableWidth / CGFloat(numberOfColumns)
            
            if !isAnswer {
                columnWidth = availableWidth
                xOffset[column] = 0
            }
            
            let fromRect = CGRect(x: 0, y: 0,
                                  width: columnWidth, height: rect.height)
            
            let itemSize = viewModel.size(forItemIn: fromRect)
            
            var frame = CGRect(x: xOffset[column], y: yOffset[column],
                               width: columnWidth, height: itemSize.height)
            
            frame.origin.x += contentInset.left
            
            y = max(y, frame.maxY)
            
            yOffset[column] += itemSize.height
            
            if !isAnswer {
                yOffset = [CGFloat](repeating: yOffset[column], count: numberOfColumns)
                column = numberOfColumns
            }
            
            cellFrames.append(frame)
            
            let sectionLimit = SectionLimit(top: sectionTop,
                                            bottom: y,
                                            type: viewModel.type)
            
            sectionLimits.append(sectionLimit)
            
            column = column >= (numberOfColumns - 1) ? 0 : (column + 1)
        }
    }
    
    override func prepare() {
        guard let collectionView = collectionView else { return }
        
        prepareContentSize(collectionView)

        previousAttributes = currentAttributes

        currentContentSize = CGSize.zero
        currentAttributes = []

        let availableWidth = collectionView.bounds.inset(by: collectionView.layoutMargins).width
        let availableHeight = collectionView.bounds.inset(by: collectionView.layoutMargins).height
        
        for itemIndex in 0..<delegate.numberOfItems {
            let viewModel = delegate.viewModel(for: itemIndex)
            let indexPath = IndexPath(item: itemIndex, section: 0)
            let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
            
            var frame = cellFrames[itemIndex]
            
            if let _ = viewModel as? ButtonCellViewModel {
                let frameMaxY = availableHeight - frame.height
                frame.origin.y = frameMaxY
            }

            if let _ = viewModel as? AnswerCellViewModel {
                let sectionLimitTop = sectionLimits.last?.bottom ?? 0.0
                let frameY: CGFloat = collectionView.isCentered ? ((availableHeight - sectionLimitTop) / 2) : 50
                frame.origin.y += frameY
            }

            attributes.frame = frame
            
            currentAttributes.append(attributes)
        }

        currentContentSize = CGSize(width: availableWidth, height: availableHeight)
    }
    
    // MARK: - Layout Attributes

    override func initialLayoutAttributesForAppearingItem(at itemIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        guard previousAttributes.indices.contains(itemIndexPath.item) else { return nil }
        return previousAttributes[itemIndexPath.item]
    }

    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        guard currentAttributes.indices.contains(indexPath.item) else { return nil }
        return currentAttributes[indexPath.item]
    }

    override func finalLayoutAttributesForDisappearingItem(at itemIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return layoutAttributesForItem(at: itemIndexPath)
    }

    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        return currentAttributes.filter { rect.intersects($0.frame) }
    }

    // MARK: - Invalidation

    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }

    // MARK: - Collection View Info

    override var collectionViewContentSize : CGSize {
        return currentContentSize
    }
}
