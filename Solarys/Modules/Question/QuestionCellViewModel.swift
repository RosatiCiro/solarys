//
//  QuestionCellViewModel.swift
//  Solarys
//
//  Created by Marcello De Palo on 11/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

class QuestionCellViewModel: NSObject, ViewModel {

    // MARK: - Private

    fileprivate enum CodingKeys: CodingKey {
        case title
        case subtitle
    }

    fileprivate var model: Question!

    fileprivate(set) var cell: [CellRepresentable.Type] = []

    fileprivate(set) var viewModelsTypes: [CellRepresentable.Type] = []
    
    fileprivate var button: ButtonCellViewModel? {
        return viewModels.first(where: { $0.type == ButtonCellViewModel.self }) as? ButtonCellViewModel
    }

    fileprivate var answers: [AnswerCellViewModel] {
        return viewModels.filter({ $0.type == AnswerCellViewModel.self }) as? [AnswerCellViewModel] ?? [AnswerCellViewModel]()
    }

    fileprivate var viewModels = [CellRepresentable]()

    // MARK: - Properties

    /// The correct answer ID
    var correntAnswerNumber: Int { model.correctAnswerNumber }
    
    /// The correct answer text
    var correctAnswerText: String { model.correctAnswerText }

    /// The current answer ID
    var currentAnswerNumber: Int = 0

    var currentIndexPath = IndexPath(item: 0, section: 0)
    
    var isCompleted: Bool = false
    
    var layout: QuestionLayout!

    var type: CellRepresentable.Type = QuestionCellViewModel.self
    
    /// The number of columns in the layout system
    var numberOfColumns: Int {
        let columns = model.numberOfColumns
        if UIDevice.current.isPad {
            return answers.count == 3 ? 3 : 2
        }
        return columns
    }

    /// The number of columns in the layout system
    var numberOfItems: Int {
        return viewModels.count
    }

    // MARK: - Events
    
    var didError: ((Error) -> Void)?
    var didSelect: ((AnyObject?) -> Void)?
    var didUpdate: ((Any) -> Void)?
    var didReloadData: (() -> Void)?
    var didShowBanner: (() -> Void)?

    // MARK: - Lifecycle
    
    required init(model: Any) {
        super.init()
        guard let model = model as? Question else { return }
        self.model = model
        
        reloadData()
    }
    
    required init(from decoder: Decoder) throws {
        super.init()
        self.model = try Question(from: decoder)
        reloadData()
    }

    deinit { }

    private func reloadData() {
        layout = QuestionLayout()
        layout.delegate = self
        
        viewModels = model.modules.compactMap { self.viewModelFor($0) }
        
        button?.didUpdate = { [weak self] element in
            guard let `self` = self else { return }
            
            self.isCompleted = self.currentAnswerNumber == self.correntAnswerNumber

            if self.isCompleted {
                // move to the next question
                //return
            }
            
            self.didShowBanner?()
       }
    }

    fileprivate func viewModelFor(_ module: Module) -> CellRepresentable? {
        guard let cellRepresentable = module.cellRepresentable
            else { return nil }

        viewModelsTypes.append(cellRepresentable.type)
        return cellRepresentable
    }
    
    /// Return CellRepresentable by index
    ///
    internal func viewModel(for index: Int) -> CellRepresentable {
        return viewModels[index]
    }
    
    /// Reset data for restart the current question
    ///
    func reset() {
        currentAnswerNumber = 0
        isCompleted = false
    }
}

// MARK: - CellRepresentable

extension QuestionCellViewModel: CellRepresentable {
    
    static func registerCell(_ collectionView: UICollectionView) {
        collectionView.register(QuestionCell.self)
    }
    
    func dequeueCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell? {
        let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as QuestionCell
        cell.setup(self)
        return cell
    }

    func selected() { /* */ }
    
    func size() -> CGSize {
        return size(forItemIn: .zero)
    }
    
    func size(forItemIn rect: CGRect) -> CGSize {
        return rect.size
    }
}

// MARK: - UICollectionViewDataSource

extension QuestionCellViewModel: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModels.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = viewModels[indexPath.item].dequeueCell(collectionView, cellForItemAt: indexPath)
        return cell ?? UICollectionViewCell()
    }
}

// MARK: - UICollectionViewDelegate

extension QuestionCellViewModel : UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard
            let vm = viewModels[indexPath.item] as? AnswerCellViewModel
            else {
                return
        }
        
        currentAnswerNumber = vm.id
        
        currentIndexPath = indexPath
        
        // Change selection state for answer
        collectionView.selectItem(at: indexPath, animated: false, scrollPosition: .centeredHorizontally)
        
        // Enable button
        button?.didEnable?(true)
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
    }
}

// MARK: - UICollectionViewDelegateFlowLayout

extension QuestionCellViewModel : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return viewModels[indexPath.item].size(forItemIn: collectionView.frame)
    }
}
