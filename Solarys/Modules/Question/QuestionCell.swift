//
//  QuestionCell.swift
//  Solarys
//
//  Created by Marcello De Palo on 11/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit
import AVFoundation

class QuestionCell: UICollectionViewCell, NibLoadableView {
    
    // MARK: - IBOutlets

    @IBOutlet fileprivate weak var collectionView: UICollectionView!

    // MARK: - Private

    /// The feedback for the user if se Answer is correct or wrong
    fileprivate lazy var answerView: AnswerView = {
        let view = UIView.loadFromNibNamed(nibNamed: "AnswerView") as! AnswerView
        view.frame = UIScreen.main.bounds
        view.configure(status: .success)
        return view
    }()

    fileprivate weak var delegate: QuestionCellViewModel!
    
    // MARK: - mvvm
    
    func setup(_ viewModel: QuestionCellViewModel) {
        delegate = viewModel
        
        answerView.removeFromSuperview()

        delegate.currentAnswerNumber = 0
        delegate.isCompleted = false

        viewModel.didReloadData = reloadData
        viewModel.didShowBanner = showBanner
        
        viewModel.viewModelsTypes.forEach { $0.registerCell(self.collectionView) }

        if let layout = collectionView.collectionViewLayout as? QuestionLayout {
            layout.delegate = viewModel
            collectionView.isCentered = true
        }

        collectionView.delegate = viewModel
        collectionView.dataSource = viewModel
        collectionView.reloadData()
    }
        
    fileprivate func showBanner() {
        let status: AnswerStatus = delegate.isCompleted ? .success : .wrong
          
        collectionView.isCentered = UIDevice.current.isPad ? true : false

        UIView.animate(
            withDuration: 0.4,
            delay: 0.0,
            usingSpringWithDamping: 1.0,
            initialSpringVelocity: 0.0,
            options: UIView.AnimationOptions(),
            animations: {
                self.collectionView.collectionViewLayout.invalidateLayout()
                self.collectionView.layoutIfNeeded()
                self.delegate.didUpdate?(ApplicationUpdate.layout)
            },
            completion: nil
        )

        addSubview(answerView)

        answerView.frame = UIScreen.main.bounds
        answerView.configure(answer: delegate.correctAnswerText, status: status)
        answerView.animate()
        
        let sound = delegate.isCompleted ? 1035 : 1050
        AudioServicesPlaySystemSound (SystemSoundID(sound))
        
        let generator = UIImpactFeedbackGenerator(style: .heavy)
        generator.impactOccurred()
    }
    
    /// Reload data after updates
    ///
    fileprivate func reloadData() {
        collectionView.reloadData()
    }
    
    // MARK: - Layout

    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
        layoutIfNeeded()
    }
}
