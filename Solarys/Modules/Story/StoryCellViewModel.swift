//
//  StoryCellViewModel.swift
//  Solarys
//
//  Created by Marcello De Palo on 11/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

class StoryCellViewModel: NSObject, ViewModel {

    // MARK: - Private

    fileprivate var app: Application?

    fileprivate var model: Story!
    
    fileprivate(set) var viewModelsTypes: [CellRepresentable.Type] = []
    
    fileprivate(set) var viewModels = [CellRepresentable]()
    
    // MARK: - Properties

    var type: CellRepresentable.Type = StoryCellViewModel.self

    var height: Double? { model.height }

    var title: String { model.title }

    var scrollToItem: Bool = true

    // MARK: - Events
    
    var didError: ((Error) -> Void)?
    var didSelect: ((AnyObject?) -> Void)?
    var didUpdate: ((Any) -> Void)?
    var didUpdateCentered: (() -> Void)?

    // MARK: - Lifecycle
    
    required init(model: Any) {
        super.init()
        guard let model = model as? Story else { return }
        self.model = model
        
        reloadData()
    }
    
    required init(from decoder: Decoder) throws {
        super.init()
        self.model = try Story(from: decoder)
        
        reloadData()
    }

    deinit { }
    
    fileprivate func reloadData() {
        viewModels = model.modules.compactMap { self.viewModelFor($0) }        
    }
    
    fileprivate func viewModelFor(_ module: Module) -> CellRepresentable? {
        guard
            let cell = module.cellRepresentable,
            var viewModel = cell as? ViewModel
            else { return nil }
        
        viewModel.didSelect = { [weak self] element in
            guard let `self` = self else { return }
            self.didSelect?(element)
        }

        viewModel.didUpdate = { [weak self] element in
            guard let `self` = self else { return }
            self.didUpdate?(self)
        }

        viewModelsTypes.append(cell.type)
        return cell
    }
}

// MARK: - CellRepresentable

extension StoryCellViewModel: CellRepresentable {
    
    static func registerCell(_ collectionView: UICollectionView) {
        collectionView.register(StoryCell.self)
    }
    
    func dequeueCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell? {
        let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as StoryCell
        cell.setup(self)
        return cell
    }

    func selected() { /* */ }
    
    func size() -> CGSize {
        return size(forItemIn: .zero)
    }
    
    func size(forItemIn rect: CGRect) -> CGSize {
        guard let height = model.height else {
            return rect.size
        }

        var size = rect.size
        size.height = CGFloat(height)

        return size
    }
}

// MARK: - UICollectionViewDataSource

extension StoryCellViewModel: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = viewModels[indexPath.item].dequeueCell(collectionView, cellForItemAt: indexPath)
        return cell ?? UICollectionViewCell()
    }
}

// MARK: - UICollectionViewDelegate

extension StoryCellViewModel : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModels[indexPath.item].selected()
    }
}

// MARK: - UICollectionViewDelegateFlowLayout

extension StoryCellViewModel : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {        
        return viewModels[indexPath.item].size(forItemIn: collectionView.frame)
    }
}

// MARK: - Scroll View Delegate

extension StoryCellViewModel: UIScrollViewDelegate {

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    }
}
