//
//  StoryCell.swift
//  Solarys
//
//  Created by Marcello De Palo on 11/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

class StoryCell: UICollectionViewCell, NibLoadableView {
    
    // MARK: - IBOutlets

    @IBOutlet fileprivate weak var collectionView: UICollectionView!

    // MARK: - Private

    fileprivate weak var delegate: StoryCellViewModel!

    // MARK: - Initialization
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        guard
            let delegate = delegate,
            delegate.viewModelsTypes.count > 1,
            delegate.scrollToItem else { return }
        
        let index = delegate.viewModelsTypes.count / 2
        collectionView.scrollToItem(at: IndexPath(item: index, section: 0),
                                    at: .centeredHorizontally,
                                    animated: false)
    }

    // MARK: - mvvm
    
    func setup(_ viewModel: StoryCellViewModel) {
        delegate = viewModel
        
        viewModel.viewModelsTypes.forEach { $0.registerCell(self.collectionView) }
        
        let layout = StoryMosaicLayout()
        layout.delegate = self
        
        collectionView.collectionViewLayout = layout

        collectionView.delegate = viewModel
        collectionView.dataSource = viewModel
        
        collectionView.setContentOffset(collectionView.contentOffset, animated:false)
    }
    
    // MARK: - Layout

    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
        layoutIfNeeded()
    }
}

extension StoryCell: StoryMosaicLayoutDelegate {
    func collectionView(_ collectionView: UICollectionView, heightForItemAt index: Int) -> CGFloat {
        return delegate.viewModels[index].size(forItemIn: collectionView.frame).height
    }
    
    func segmentStyle(_ itemIndex: Int) -> StoryMosaicSegmentStyle {
        guard let vm = delegate.viewModels[itemIndex] as? TopicCellViewModel else { return .fullWidth }
        return StoryMosaicSegmentStyle(rawValue: vm.segment) ?? .fullWidth
    }
}

extension StoryCell {
    var collectionViewOffset: CGFloat {
        set {
            collectionView.contentOffset.x = newValue
        }
        get {
            return collectionView.contentOffset.x
        }
    }
}

