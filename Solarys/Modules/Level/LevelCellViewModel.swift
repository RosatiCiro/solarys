//
//  LevelCellViewModel.swift
//  Solarys
//
//  Created by Marcello De Palo on 11/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

class LevelCellViewModel: NSObject, ViewModel {

    // MARK: - Private

    private enum CodingKeys: CodingKey {
        case title
        case subtitle
    }

    fileprivate var model: Level!

    fileprivate(set) var viewModelsTypes: [CellRepresentable.Type] = []
    
    fileprivate(set) var viewModels = [CellRepresentable]()
        
    // MARK: - Properties

    var type: CellRepresentable.Type = LevelCellViewModel.self

    internal var percentage: Double = 0 {
        didSet {
            self.model.percentage = percentage
            self.didUpdate?(ApplicationUpdate.level(level: model))
        }
    }
    
    var story: Int { model.story }
    var topic: Int { model.topic }
    var level: Int { model.level }
    var isCompleted: Bool { model.isComplete }
    
    // MARK: - Events
    
    var didError: ((Error) -> Void)?
    var didSelect: ((AnyObject?) -> Void)?
    var didUpdate: ((Any) -> Void)?
    var didUpdateProgress: ((Double) -> Void)?
    var didComplete: (() -> Void)?
    var didUpdateToNext: ((Int) -> Void)?

    // MARK: - Lifecycle
        
    required init(from decoder: Decoder) throws {
        super.init()
        self.model = try Level(from: decoder)
        
        configure()
    }

    deinit { }
    
    fileprivate func configure() {
        model.isComplete = isComplete()
        viewModels = model.modules.compactMap { self.viewModelFor($0) }
    }

    fileprivate func viewModelFor(_ module: Module) -> CellRepresentable? {
        guard
            let cellRepresentable = module.cellRepresentable,
            var viewModel = cellRepresentable as? ViewModel
            else { return nil }
        
        viewModel.didUpdate = { [weak self] element in
            guard let `self` = self else { return }
            
            if let value = element as? QuestionsCellViewModel {
                self.percentage = value.percentage
            } else if let _ = element as? Double {
                self.didUpdateToNext?(1)
            } else {
                self.didUpdate?(element)
            }
        }

        viewModel.didSelect = { [weak self] element in
            guard let `self` = self else { return }
            self.didSelect?(element)
        }
        viewModel.didError = { [weak self] error in
            self?.didError?(error)
        }

        viewModelsTypes.append(cellRepresentable.type)

        return cellRepresentable
    }
    
    fileprivate func isComplete() -> Bool {
        let data = DataManager.shared
            .fetchHistory(story: model.story,
                          topic: model.topic,
                          level: model.level)
        return data.count > 0
    }
    
    // API
    
    func reset() {
        model.isComplete = false
    }
}

// MARK: - CellRepresentable

extension LevelCellViewModel: CellRepresentable {

    static func registerCell(_ collectionView: UICollectionView) {
        collectionView.register(LevelCell.self)
    }
    
    func dequeueCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell? {
        let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as LevelCell
        cell.setup(self)
        return cell
    }

    func selected() { /* */ }
    
    func size() -> CGSize {
        return size(forItemIn: .zero)
    }
    
    func size(forItemIn rect: CGRect) -> CGSize {
        return rect.size
    }
}

// MARK: - UICollectionViewDataSource

extension LevelCellViewModel: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = viewModels[indexPath.item].dequeueCell(collectionView, cellForItemAt: indexPath)
        return cell ?? UICollectionViewCell()
    }
}

// MARK: - UICollectionViewDelegate

extension LevelCellViewModel : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModels[indexPath.item].selected()
    }
}

// MARK: - UICollectionViewDelegateFlowLayout

extension LevelCellViewModel : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return viewModels[indexPath.item].size(forItemIn: collectionView.frame)
    }
}

