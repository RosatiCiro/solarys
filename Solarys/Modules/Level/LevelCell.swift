//
//  LevelCell.swift
//  Solarys
//
//  Created by Marcello De Palo on 11/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

class LevelCell: UICollectionViewCell, NibLoadableView {
    
    // MARK: - IBOutlets

    @IBOutlet fileprivate weak var collectionView: UICollectionView!
    
    // MARK: - Private

    fileprivate weak var delegate: LevelCellViewModel!

    // MARK: - mvvm
    
    func setup(_ viewModel: LevelCellViewModel) {
        delegate = viewModel
        
        viewModel.viewModelsTypes.forEach { $0.registerCell(self.collectionView) }

        viewModel.didUpdateToNext = didUpdateToNext

        collectionView.delegate = viewModel
        collectionView.dataSource = viewModel
    }
    
    /// Move to next
    ///
    fileprivate func didUpdateToNext(index: Int) {
        guard index < delegate.viewModels.count
            else {
                delegate.didUpdate?(ApplicationUpdate.topicDone)
                return
        }
        collectionView.scrollToItem(at: IndexPath(item: index, section: 0),
                                    at: .centeredHorizontally,
                                    animated: true)
    }
    
    // MARK: - Layout

    override func layoutSubviews() {
        super.layoutSubviews()
        collectionView.collectionViewLayout.invalidateLayout()
    }
    
    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
        layoutIfNeeded()
    }
}
