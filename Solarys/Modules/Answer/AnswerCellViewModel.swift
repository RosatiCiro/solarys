//
//  AnswerCellViewModel.swift
//  Solarys
//
//  Created by Marcello De Palo on 11/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

class AnswerCellViewModel: ViewModel {

    // MARK: - Private

    private enum CodingKeys: CodingKey {
        case title
        case subtitle
    }

    fileprivate var model: Answer!

    // MARK: - Properties

    var type: CellRepresentable.Type = AnswerCellViewModel.self
    
    var id: Int { return model.id }

    var body: String { return model.body }

    var image: String? { return model.image }

    var isSelected: Bool = false {
        willSet {
            model.isSelected = newValue
        }
        didSet {
            //didUpdate?(model) --> ViewModelUpdates.layout
        }
    }

    // MARK: - Lifecycle
    
    required init(model: Any) {
        guard let model = model as? Answer else { return }
        self.model = model
    }
    
    required init(from decoder: Decoder) throws {
        self.model = try Answer(from: decoder)
    }

    deinit { }

    // MARK: - Events
    
    var didError: ((Error) -> Void)?
    var didSelect: ((AnyObject?) -> Void)?
    var didUpdate: ((Any) -> Void)?
}

// MARK: - CellRepresentable

extension AnswerCellViewModel: CellRepresentable {
    
    static func registerCell(_ collectionView: UICollectionView) {
        collectionView.register(AnswerCell.self)
    }
    
    func dequeueCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell? {
        let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as AnswerCell
        cell.setup(self)
        return cell
    }

    func selected() {
        isSelected = !isSelected
        
    }
    
    func size() -> CGSize {
        return size(forItemIn: .zero)
    }
    
    func size(forItemIn rect: CGRect) -> CGSize {
        if let cell = UIView.loadFromNibNamed(nibNamed: "AnswerCell") as? AnswerCell {

            cell.prepareForReuse()
            cell.setup(self)
    
            cell.setNeedsUpdateConstraints()
            cell.updateConstraintsIfNeeded()
    
            cell.bounds = CGRect(x: 0.0, y: 0.0, width: rect.width, height: cell.bounds.height);
    
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
    
            let height = cell.contentView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
            
            if UIDevice.current.isPad && height < AnswerCell.kPadMinCellHeight {
                return CGSize(width: rect.width, height: AnswerCell.kPadMinCellHeight)
            }

            
            return CGSize(width: rect.width, height: height)
        }
        return CGSize(width: rect.width, height: 70)
    }
}

// MARK: - Hashable

extension AnswerCellViewModel: Hashable {
    
    static func == (lhs: AnswerCellViewModel, rhs: AnswerCellViewModel) -> Bool {
        return (lhs.model.id == rhs.model.id)
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(model.id)
    }
}
