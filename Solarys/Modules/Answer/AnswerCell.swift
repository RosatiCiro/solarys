//
//  AnswerCell.swift
//  Solarys
//
//  Created by Marcello De Palo on 11/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

class AnswerCell: UICollectionViewCell, NibLoadableView {
    
    // MARK: - Public static

    // The min cell height size for iPad version
    static let kPadMinCellHeight: CGFloat = 150

    // MARK: - IBOutlets
    
    @IBOutlet weak var selectionView: UIView!
    
    @IBOutlet weak var bodyLabel: UILabel!

    @IBOutlet weak var imageView: UIImageView!

    @IBOutlet fileprivate weak var imageViewTopLayoutConstraint: NSLayoutConstraint!

    @IBOutlet fileprivate weak var imageViewHeightLayoutConstraint: NSLayoutConstraint!

    @IBOutlet fileprivate weak var widthLayoutConstraint: NSLayoutConstraint!

    // MARK: - Initialization
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        widthLayoutConstraint.constant = bounds.inset(by: layoutMargins).width
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageViewTopLayoutConstraint.constant = 0
        imageViewHeightLayoutConstraint.constant = 0
    }

    // MARK: - mvvm

    func setup(_ viewModel: AnswerCellViewModel) {
                
        bodyLabel.textColor = .answerText
        bodyLabel.text = viewModel.body
        
        selectionView.layer.cornerRadius = 10
        selectionView.layer.borderColor = UIColor.answerBorder.cgColor
        selectionView.layer.borderWidth = 1
        selectionView.layer.masksToBounds = true
        selectionView.layer.backgroundColor = UIColor.answerBackground.cgColor
        
        imageViewTopLayoutConstraint.constant = 0
        imageViewHeightLayoutConstraint.constant = 0
        
        guard
            let imageString = viewModel.image,
            let image = UIImage(named: imageString)
            else { return }
     
        imageView.image = image
        imageViewTopLayoutConstraint.constant = 15.0
        imageViewHeightLayoutConstraint.constant = UIDevice.current.isPad ? 100 : 60
    }
    
    // MARK: - Layout

    override func layoutSubviews() {
        super.layoutSubviews()
        widthLayoutConstraint.constant = bounds.inset(by: layoutMargins).width
    }

    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
        layoutIfNeeded()
    }
    
    // MARK: - UICollectionReusableView
    
    override var isSelected: Bool{
        didSet {
            let borderColor: UIColor = self.isSelected ? UIColor.selected.withAlphaComponent(0.5) : .answerBorder
            let backgroundColor = self.isSelected ? borderColor.withAlphaComponent(0.1) : .answerBackground
            
            selectionView.layer.borderColor = borderColor.cgColor
            selectionView.layer.backgroundColor = backgroundColor.cgColor
                        
            super.isSelected = self.isSelected
        }
    }
}
