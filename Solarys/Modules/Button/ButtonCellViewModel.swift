//
//  ButtonCellViewModel.swift
//  Solarys
//
//  Created by Marcello De Palo on 02/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

class ButtonCellViewModel: ViewModel {

    // MARK: - Private

    private enum CodingKeys: CodingKey {
        case text
        case isEnabled
        case color
    }
    
    fileprivate var model: Button!

    // MARK: - Properties

    var type: CellRepresentable.Type = ButtonCellViewModel.self

    var text: String { model.text }
    
    var isEnabled: Bool { model.isEnabled }

    var color: String? { model.color }

    var endpoint: String? { model.endpoint }

    // MARK: - Lifecycle
    
    required init(model: Any) {
        guard let model = model as? Button else { return }
        self.model = model
    }
    
    required init(from decoder: Decoder) throws {
        self.model = try Button(from: decoder)
    }

    deinit { }

    // MARK: - Events
    
    var didError: ((Error) -> Void)?
    var didSelect: ((AnyObject?) -> Void)?
    var didUpdate: ((Any) -> Void)?
    var didEnable: ((Bool) -> Void)?
}

// MARK: - CellRepresentable

extension ButtonCellViewModel: CellRepresentable {
    
    static func registerCell(_ collectionView: UICollectionView) {
        collectionView.register(ButtonCell.self)
    }
    
    func dequeueCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell? {
        let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as ButtonCell
        cell.setup(self)
        return cell
    }

    func selected() {
        print("")
    }
    
    func size() -> CGSize {
        return size(forItemIn: .zero)
    }
    
    func size(forItemIn rect: CGRect) -> CGSize {
        return CGSize(width: rect.width, height: 80)
    }
}
