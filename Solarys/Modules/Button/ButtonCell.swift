//
//  ButtonCell.swift
//  Solarys
//
//  Created by Marcello De Palo on 02/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

class ButtonCell: UICollectionViewCell, NibLoadableView {
    
    // MARK: - IBOutlets

    @IBOutlet fileprivate weak var button: UIButton!

    // MARK: private
    
    fileprivate var delegate: ButtonCellViewModel!
    
    // MARK: - Initialization
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }

    // MARK: - mvvm
    
    func setup(_ viewModel: ButtonCellViewModel) {
        delegate = viewModel
        
        viewModel.didEnable = didEnable

        button.setTitle(viewModel.text.localized(), for: .normal)
        
        button.setTitleColor(.white, for: .normal)
        button.setTitleColor(.lightGray, for: .disabled)
        //button.setTitleColor(.white, for: .selected)
                
        button.layer.cornerRadius = 10
        button.layer.masksToBounds = false
        
        didEnable(isEnabled: viewModel.isEnabled)
    }
    
    fileprivate func didEnable(isEnabled: Bool) {
        
        var borderColor: UIColor = isEnabled ? UIColor.systemGreen : .buttonDisabled
        var backgroundColor = isEnabled ? borderColor : .buttonDisabled
        
        if let color = delegate.color, let uiColor = UIColor(named: color) {
            borderColor = uiColor
            backgroundColor = uiColor
        }
        
        button.isEnabled = isEnabled
        button.layer.borderColor = borderColor.cgColor
        button.layer.backgroundColor = backgroundColor.cgColor
    }
    
    @IBAction func buttonTapped(_ sender: UIButton) {
        guard let endpoint = delegate.endpoint, endpoint == "ar"
            else {
                delegate.didUpdate?(ApplicationUpdate.layout)
                return
        }
        
        delegate.didUpdate?(ApplicationUpdate.levelDone)

        //delegate.didSelect?(nil)
    }
    
    // MARK: - Layout

    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
        layoutIfNeeded()
    }
}
