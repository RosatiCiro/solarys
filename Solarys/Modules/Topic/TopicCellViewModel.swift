//
//  TopicCellViewModel.swift
//  Solarys
//
//  Created by Marcello De Palo on 11/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

class TopicCellViewModel: ViewModel {

    // MARK: - Private

    private enum CodingKeys: CodingKey {
        case text
        case image
        case color
        case segment
    }
    
    fileprivate var model: Topic!

    // MARK: - Properties

    var type: CellRepresentable.Type = TopicCellViewModel.self

    /// The story ID
    var story: Int { return model.story }
    
    /// The topic ID
    var topic: Int { return model.topic }
    
    /// The topic name
    var name: String { return model.name }
    
    /// The color of the topic
    var color: String { return model.color }

    /// The image of the topic
    var image: String { return model.image }

    /// The text color for the topic
    var textColor: String? { model.textColor }

    /// The layout type in the story module (fullWidth, fiftyFifty, oneThird)
    var segment: String { model.segment }
    
    /// The status of the topic if is loked
    var isLocked: Bool { model.isLocked }

    /// The parents are th unlokers for the topic
    var parents: Parents? { model.parents }

    /// The percentage of the completion of the topic
    var percentage: Double {
        guard self.model.percentage == nil else { return self.model.percentage ?? 0.0 }
        guard self.model.numberOfLevels > 0 else { return 0 }
        return  Double((100 / self.model.numberOfLevels) * self.model.numberOfLevelsCompleted) / 100
    }

    /// The status of the topic if is coplete
    var isComplete: Bool { return model.isComplete }

    // MARK: - Events
    
    var didError: ((Error) -> Void)?
    var didSelect: ((AnyObject?) -> Void)?
    var didUpdate: ((Any) -> Void)?

    // MARK: - Lifecycle
    
    required init(model: Any) {
        guard let model = model as? Topic else { return }
        self.model = model
    }
    
    required init(from decoder: Decoder) throws {
        self.model = try Topic(from: decoder)
    }

    deinit { }
}

// MARK: - CellRepresentable

extension TopicCellViewModel: CellRepresentable {
    
    static func registerCell(_ collectionView: UICollectionView) {
        collectionView.register(TopicCell.self)
    }
    
    func dequeueCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell? {
        let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as TopicCell
        cell.setup(self)
        return cell
    }

    func selected() {
        guard isLocked == false else { return }
        didSelect?(model)
    }
    
    func size() -> CGSize {
        return size(forItemIn: .zero)
    }
    
    func size(forItemIn rect: CGRect) -> CGSize {
        return TopicCell.size()
    }
}
