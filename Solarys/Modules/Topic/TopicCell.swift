//
//  TopicCell.swift
//  Solarys
//
//  Created by Marcello De Palo on 11/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

class TopicCell: UICollectionViewCell, NibLoadableView {
    
    // MARK: - IBOutlets

    @IBOutlet private weak var textLabel: UILabel!

    @IBOutlet private weak var imageView: UIImageView!

    @IBOutlet private weak var progressView: ProgressView!

    // MARK: private
    
    // MARK: - Initialization
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }

    // MARK: - mvvm
    
    func setup(_ viewModel: TopicCellViewModel) {
        
        textLabel.text = viewModel.name.localized()
        textLabel.font = .for(style: .body2)

        if let image = UIImage(named: viewModel.image) {
            imageView.image = viewModel.isLocked ? image.grayScale() : image
        } else {
            imageView.backgroundColor = .from(hexString: viewModel.color)
            imageView.layer.cornerRadius = imageView.frame.height / 2
            imageView.isHidden = false
        }
        
        progressView.progress = CGFloat(viewModel.percentage)
        progressView.lineWidth = 6
        progressView.borderOffset = 0        
    }
    
    // MARK: - Layout

    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
        layoutIfNeeded()
    }
    
    static func size() -> CGSize {
        return CGSize(width: 100, height: 160)
    }
}
