//
//  PlanetCellViewModel.swift
//  Solarys
//
//  Created by Marcello De Palo on 11/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

class PlanetCellViewModel: ViewModel {
    
    // MARK: - Private

    private enum CodingKeys: CodingKey {
        case name
        case color
        case image
    }

    fileprivate var model: Planet!

    // MARK: - Properties
    
    var type: CellRepresentable.Type = PlanetCellViewModel.self
    
    /// The planet name
    var name: String { return model.name }
    
    /// The color of the planet
    var color: String { return model.color }

    /// The image of the planet
    var image: String { return model.image }

    var textColor: String? { model.textColor }

    var percentage: Double {
        guard self.model.percentage == nil else { return self.model.percentage ?? 0.0 }
        guard self.model.numberOfLevels > 0 else { return 0 }
        return  Double((100 / self.model.numberOfLevels) * self.model.numberOfLevelsCompleted) / 100
    }

    var isComplete: Bool { return model.isComplete }
    
    // MARK: - Events
    
    var didError: ((Error) -> Void)?
    var didSelect: ((AnyObject?) -> Void)?
    var didUpdate: ((Any) -> Void)?
    
    // MARK: - Lifecycle
    
    required init(model: Any) {
        guard let model = model as? Planet else { return }
        self.model = model
        
    }
    
    required init(from decoder: Decoder) throws {
        self.model = try Planet(from: decoder)
    }

    deinit { }

    // MARK: - API

    func planet() -> Planet {
        return model
    }
}

// MARK: - CellRepresentable

extension PlanetCellViewModel: CellRepresentable {
    
    static func registerCell(_ collectionView: UICollectionView) {
        collectionView.register(PlanetCell.self)
    }
    
    func dequeueCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell? {
        let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as PlanetCell
        cell.setup(self)
        return cell
    }

    func selected() {
        didSelect?(model)
    }
    
    func size() -> CGSize {
        return PlanetCell.size()
    }
}
