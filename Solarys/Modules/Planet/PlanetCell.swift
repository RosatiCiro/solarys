//
//  PlanetCell.swift
//  Solarys
//
//  Created by Marcello De Palo on 11/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

class PlanetCell: UICollectionViewCell, NibLoadableView {
    
    // MARK: - IBOutlets

    @IBOutlet private weak var planetLabel: UILabel!

    @IBOutlet private weak var planetView: UIImageView!

    @IBOutlet private weak var progressView: ProgressView!

    // MARK: - Initialization
    
    var index: CGFloat = 0.0
    
    override func prepareForReuse() {
        super.prepareForReuse()
        UIView.performWithoutAnimation {
            progressView.progress = 0
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: - mvvm
    
    func setup(_ viewModel: PlanetCellViewModel) {
        
        planetLabel.text = viewModel.name.localized()
        planetLabel.font = .for(style: .h2)
        
        if let color = viewModel.textColor, let uiColor = UIColor(named: color) {
            planetLabel.textColor = uiColor
        }

        if let image = UIImage(named: viewModel.name.lowercased()) {
            planetView.image = image
        } else {
            planetView.backgroundColor = .from(hexString: viewModel.color)
            planetView.layer.cornerRadius = planetView.frame.height / 2
            planetView.isHidden = false
        }
        
        progressView.progress = CGFloat(viewModel.percentage)
    }
    
    // MARK: - Layout

    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
        layoutIfNeeded()
    }
    
    static func size() -> CGSize {
        return CGSize(width: 220, height: 350)
    }
}
