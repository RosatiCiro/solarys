//
//  Application.swift
//  Solarys
//
//  Created by Marcello De Palo on 11/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

enum ApplicationUpdate {
    case layout
    case data
    case ar
    case topicDone
    case level(level: Level)
    case levelDone
    //case `default`
}


class Application {
       
    fileprivate let window: UIWindow
    
    lazy var navigation: Navigation = Navigation(
        window: self.window,
        navigationController: NavigationController(),
        application: self
    )
    
    // MARK: - Lifecycle

    init(window: UIWindow) {
        self.window = window

        //DataManager.shared.reset("History")
        DataManager.reset()

        /*
         
         For debug ---
         
        DataManager.shared.reset("History")
        DataManager.reset()

        let data = [
            ["story": 0, "topic": 0, "level": 1] as [String : AnyObject],
            ["story": 0, "topic": 0, "level": 2] as [String : AnyObject],
            ["story": 0, "topic": 4, "level": 1] as [String : AnyObject],
            ["story": 0, "topic": 4, "level": 2] as [String : AnyObject]
        ]
        for element in data {
            DataManager.shared.store(element, for: "History")
        }
        
        let levels = DataManager.shared.fetchHistory()
        for level in levels {
            print("\(level.story) \(level.topic) \(level.level)")
        }
        
        if let values = DataManager.loadValue(for: "history", decodingType: [DataLevel].self) {
            let _ = DataHistory<DataLevel>(values)
            // @todo code for migration
        }
        */
    }
}
