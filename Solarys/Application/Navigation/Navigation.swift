//
//  Navigation.swift
//  Solarys
//
//  Created by Marcello De Palo on 11/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit
import AVFoundation

class Navigation {
    
    // MARK: - Private
    
    fileprivate let navigationController: UINavigationController
    
    fileprivate let application: Application
    
    // MARK: - Lifecycle
    
    init(window: UIWindow, navigationController: UINavigationController, application: Application) {
        self.application = application
        self.navigationController = navigationController
        window.rootViewController = self.navigationController
        window.makeKeyAndVisible()
    }
    
    // MARK: - Public
    
    func start() {
        self.showHome()
    }
    
    func back() {
        navigationController.popViewController(animated: true)
    }
    
    // MARK: - Private
    
    fileprivate func showHome() {
        let viewModel = HomeViewModel(application)
        viewModel.didSelect = { [weak self] (topic) in
            if topic.isComplete {
                //self?.showAR(topic)
                self?.showRestart(topic)
            } else {
                self?.showGame(topic)
            }
        }
        
        let instance = HomeViewController(viewModel: viewModel)
        navigationController.pushViewController(instance, animated: false)
    }
    
    fileprivate func showGame(_ topic: Topic, reset: Bool = false) {
        let viewModel = GameViewModel(topic, app: application)
        
        if reset {
            viewModel.reset()
        }
        
        viewModel.didComplete = { [weak self] topic in
            self?.navigationController.popViewController(animated: false)
            if let topic = topic {
                self?.showAR(topic)
            }
        }

        let instance = GameViewController(viewModel: viewModel)
        navigationController.pushViewController(instance, animated: true)
    }
    
    fileprivate func showAR(_ topic: Topic) {
        let instance = ARViewController(name: topic.name, sound: topic.sound)
        self.navigationController.styleBackBarButtonItem()

        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
            self.navigationController.pushViewController(instance, animated: true)
        } else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                if granted {
                    self.navigationController.pushViewController(instance, animated: true)
                } else {
                    //access denied
                }
            })
        }
    }
    
    fileprivate func showRestart(_ topic: Topic) {
        guard let shared = SceneDelegate.shared, let window = shared.window else { return }
        
        let istance = UIAlertController(title: nil, message: "Do you want to play again?".localized(), preferredStyle: .actionSheet)

        let playAction = UIAlertAction(title: "Yes".localized(), style: .default, handler: { (alert: UIAlertAction!) -> Void in
            self.showGame(topic, reset: true)
        })

        let arAction = UIAlertAction(title: "No".localized(), style: .destructive, handler: { (alert: UIAlertAction!) -> Void in
            self.showAR(topic)
        })

        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: { (alert: UIAlertAction!) -> Void in
          //  Do something here upon cancellation.
        })

        istance.addAction(playAction)
        istance.addAction(arAction)
        istance.addAction(cancelAction)

        if let popover = istance.popoverPresentationController {
            popover.sourceView = window
            popover.sourceRect = CGRect(x: window.bounds.midX, y: window.bounds.midY, width: 0, height: 0)
            popover.permittedArrowDirections = []
        }

        navigationController.present(istance, animated: true, completion: nil)
    }
}
