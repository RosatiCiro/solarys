//
//  History+CoreDataProperties.swift
//  Solarys
//
//  Created by Marcello De Palo on 22/01/2020.
//  Copyright © 2020 Marcello De Palo. All rights reserved.
//
//

import Foundation
import CoreData


extension History {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<History> {
        return NSFetchRequest<History>(entityName: "History")
    }

    @NSManaged public var level: Int16
    @NSManaged public var story: Int16
    @NSManaged public var topic: Int16
}
