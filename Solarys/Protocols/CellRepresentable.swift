//
//  CellRepresentable.swift
//  Solarys
//
//  Created by Marcello De Palo on 11/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

protocol CellRepresentable {

    var type: CellRepresentable.Type { get set }

    static func registerCell(_ collectionView: UICollectionView)
    func dequeueCell(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell?
    func selected()
    func size() -> CGSize
    func size(forItemIn rect: CGRect) -> CGSize
}

extension CellRepresentable {
    func size(forItemIn rect: CGRect) -> CGSize {
        return self.size()
    }
}

protocol CellHeaderRepresentable: ReusableView {
    func dequeueReusable(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionReusableView?
    func size() -> CGSize
}
