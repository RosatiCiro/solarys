//
//  LayoutDelegate.swift
//  Solarys
//
//  Created by Marcello De Palo on 21/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

protocol LayoutDelegate {
    func collectionView(_ collectionView: UICollectionView, sizeForItemAt indexPath: IndexPath, withWidth: CGFloat) -> CGSize
    func collectionView(_ collectionView: UICollectionView, viewModelForItemAt indexPath: IndexPath) -> CellRepresentable
}
