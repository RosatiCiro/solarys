//
//  ViewModel.swift
//  Solarys
//
//  Created by Marcello De Palo on 11/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import Foundation
import SwiftUI

protocol ViewModel: Decodable  {
    var id: UUID { get }
    
    var didError: ((Error) -> Void)? { get set }
    var didSelect: ((AnyObject?) -> Void)? { get set }
    var didUpdate: ((Any) -> Void)? { get set }
}

extension ViewModel {
    var id: UUID {
        return UUID()
    }
}
