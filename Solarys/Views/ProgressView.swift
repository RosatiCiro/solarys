//
//  ProgressView.swift
//  Solarys
//
//  Created by Marcello De Palo on 13/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

@IBDesignable class ProgressView: UIView {
    
    enum ProgressType {
        case circle
        case linear
    }
    
    var defaultStyle: ProgressType = .circle
    
    var progress: CGFloat = 0.0 {
        didSet{
            defaultStyle == .circle ? setNeedsDisplay() : animate()
        }
    }
    
    /// The start angle in radians
    @IBInspectable var startAngle: CGFloat = CGFloat(-90.0).toRadians()
    
    /// The end angle in radians
    @IBInspectable var endAngle: CGFloat = CGFloat(270.0).toRadians()
    
    /// The space to the border
    @IBInspectable var borderOffset: CGFloat = 5.00
    
    /// The line space for the progress view
    @IBInspectable var lineWidth: CGFloat = 10.0
    
    /// The color for the progress view
    @IBInspectable var color: UIColor = UIColor.from(hexString: "#F7B500")
    
    /// The shape for the progress bar
    fileprivate var shape: CAShapeLayer?

    fileprivate var track: CAShapeLayer?

    // MARK: - Life Cycling
    
    override func draw(_ rect: CGRect) {
        
        if let _ = shape {
            shape?.removeAllAnimations()
            shape?.removeFromSuperlayer()
            shape = nil
            shape = CAShapeLayer()
        }

        if let _ = track {
            track?.removeAllAnimations()
            track?.removeFromSuperlayer()
            track = nil
            track = CAShapeLayer()
        }
        
        let padding = defaultStyle == .circle ? borderOffset : 0

        var frameRect = rect.insetBy(dx: padding, dy: padding)
        let width = defaultStyle == .circle ? frameRect.width : frameRect.width * ((progress * 100) / 100)
        frameRect.size.width = width
        
        let shape = CAShapeLayer()
        
        let path = UIBezierPath (roundedRect: frameRect, cornerRadius: rect.height / 2).cgPath
        shape.path = path
        
        shape.lineCap = .round
        shape.strokeColor = color.cgColor
        shape.lineWidth = lineWidth
        shape.fillColor =  UIColor.clear.cgColor
        shape.strokeStart = 0.0
        shape.strokeEnd = progress

        if defaultStyle == .linear {
            shape.lineWidth = 0
            shape.lineCap = .square
            shape.fillColor = color.cgColor
            
            layer.cornerRadius = rect.height / 2
            layer.backgroundColor = UIColor.from(hexString: "#D8D8D8").cgColor
            layer.masksToBounds = true
        }
        
        self.shape = shape
        
        if defaultStyle == .circle {
            let track = CAShapeLayer()
            track.path = path
            
            track.lineCap = .round
            track.strokeColor = UIColor.progressTrack.cgColor
            track.lineWidth = lineWidth
            track.fillColor =  UIColor.clear.cgColor
            track.strokeStart = 0.0
            track.strokeEnd = 1.0
            
            self.track = track
            layer.addSublayer(track)
        }

        layer.addSublayer(shape)
        layer.masksToBounds = defaultStyle == .circle ? false : true
    }
    
    func animate() {
        let padding = defaultStyle == .circle ? borderOffset : 0

        var frameRect = self.frame.insetBy(dx: padding, dy: padding)
        let width = defaultStyle == .circle ? frameRect.width : frameRect.width * ((progress * 100) / 100)
        frameRect.size.width = width

        let path = UIBezierPath (roundedRect: frameRect, cornerRadius: frame.height / 2).cgPath

        let animation = CABasicAnimation(keyPath: "path")
        animation.duration = 0.2

        // Your new shape here
        animation.toValue = path
        animation.timingFunction = CAMediaTimingFunction(name: .easeOut)

        animation.fillMode = .forwards
        animation.isRemovedOnCompletion = false

        shape?.add(animation, forKey: nil)

    }
}
