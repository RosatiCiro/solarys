//
//  AnswerView.swift
//  Solarys
//
//  Created by Marcello De Palo on 17/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

class AnswerView: UIView {

    static let kTextViewMargins: CGFloat = 10

    static let kContentViewMinHeight: CGFloat = 250

    // MARK: - IBOutlets

    @IBOutlet fileprivate weak var groundView: UIView!
    
    @IBOutlet fileprivate weak var backgroundView: UIView!

    @IBOutlet fileprivate weak var contentView: UIView!

    @IBOutlet fileprivate weak var button: UIButton!

    @IBOutlet fileprivate weak var textView: UITextView!

    @IBOutlet fileprivate weak var contentViewHeightLayoutConstraint: NSLayoutConstraint!

    // MARK: - Init

    func configure(answer: String? = nil, status: AnswerStatus) {
        
        textView.text = answer
        
        let numberOfLines = CGFloat(textView.numberOfLines())
        let lineHeight: CGFloat = ceil(textView.font?.lineHeight ?? 26)
                
        var contentViewHeight = groundView.frame.height + (numberOfLines * lineHeight)
        
        // min value for content height
        if contentViewHeight < type(of: self).kContentViewMinHeight {
            contentViewHeight = type(of: self).kContentViewMinHeight
        }
           
        contentViewHeight += (type(of: self).kTextViewMargins * 2)
                    
        // update content height by textView text lines
        contentViewHeightLayoutConstraint.constant = contentViewHeight
        
        button.setTitle("Next".localized(), for: .normal)
        button.setTitleColor(.white, for: .normal)

        button.layer.cornerRadius = 10
        button.isEnabled = false
                
        button.layer.borderColor = status.color.cgColor
        button.layer.backgroundColor = status.color.cgColor
        button.layer.shadowColor = status.color.cgColor

        backgroundView.backgroundColor = status.color.withAlphaComponent(0.5)

        groundView.backgroundColor = status.color.withAlphaComponent(0.5)

        contentView.transform = CGAffineTransform(translationX: 0, y: contentView.frame.height)
    }
    
    // MARK: - Animate

    func animate() {
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
            self.contentView.transform = .identity
        })
    }
}
