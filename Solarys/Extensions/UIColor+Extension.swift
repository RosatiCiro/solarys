//
//  UIColor+Extension.swift
//  Solarys
//
//  Created by Marcello De Palo on 11/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

extension UIColor {

    // MARK: - Monochrome Shades
    
    static var systemWhite: UIColor {
        return UIColor(named: "systemWhite") ?? UIColor.from(hexString: "#F9F9F9")
    }
    
    // ...
    
    // MARK: - Brand Colors

    static var warning: UIColor { return UIColor(named: "warning") ?? .red }
    
    static var success: UIColor { return .systemGreen }
    
    static var danger: UIColor { return UIColor(named: "danger") ?? .orange }
    
    static var info: UIColor { return UIColor(named: "info") ?? .cyan }

    // MARK: - UI
    
    static var selected: UIColor { .info }

    static var progressTrack: UIColor {
        UIColor.from(hexString: "#EEEEEE").withAlphaComponent(0.2)
    }

    static var answerText: UIColor { UIColor.from(hexString: "#5E6E92") }
    static var answerBackground: UIColor { .white }
    static var answerBorder: UIColor { UIColor.black.withAlphaComponent(0.1) }

    static var buttonDisabled: UIColor {
        UIColor(named: "buttonDisabled") ?? UIColor.from(hexString: "#F2F2F2")
    }

    // ...
    
    // MARK: - Helpers

    /// Get color by HEX String
    ///
    /// - Parameter hexString: a String with HEX color #CCCC00
    ///
    /// - Returns: UIColor
    ///
    static func from(hexString: String) -> UIColor {
        var red = CGFloat(255)
        var green = CGFloat(255)
        var blue = CGFloat(255)

        if hexString.hasPrefix("#") {
            let newIndex = hexString.index(hexString.startIndex, offsetBy: 1)
            let start = newIndex
            let hexColor = hexString[start...]
            let scanner = Scanner(string: String(hexColor))
            var hexNumber: UInt64 = 0

            if scanner.scanHexInt64(&hexNumber) {
                red = CGFloat((hexNumber & 0xff0000) >> 16) / 255
                green = CGFloat((hexNumber & 0x00ff00) >> 8) / 255
                blue = CGFloat((hexNumber & 0x0000ff) >> 0) / 255

                return UIColor(red: red, green: green, blue: blue, alpha: 1.0)
            }
        }

        return UIColor(red: red, green: green, blue: blue, alpha: 1)
    }
    
    /// Return the color lighter
    ///
    /// - Parameter percentage: the percentage of the color lighter
    ///
    /// - Returns: UIColor
    ///
    func lighter(by percentage: CGFloat = 30.0) -> UIColor {
        return adjust(by: abs(percentage) )
    }

    /// Return the color darker
    ///
    /// - Parameter percentage: the percentage of the color darker
    ///
    /// - Returns: UIColor
    ///
    func darker(by percentage: CGFloat = 30.0) -> UIColor {
        return adjust(by: -1 * abs(percentage) )
    }

    /// Return the color adjust by percentage
    ///
    private func adjust(by percentage: CGFloat = 30.0) -> UIColor {
        var red: CGFloat = 0, green: CGFloat = 0, blue: CGFloat = 0, alpha: CGFloat = 0
        if self.getRed(&red, green: &green, blue: &blue, alpha: &alpha) {
            return UIColor(red: min(red + (percentage/100), 1.0),
                           green: min(green + (percentage/100), 1.0),
                           blue: min(blue + (percentage/100), 1.0),
                           alpha: alpha)
        } else {
            return .clear
        }
    }
}
