//
//  UINavigationController+Extensions.swift
//  Solarys
//
//  Created by Marcello De Palo on 28/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

extension UINavigationController {
    func styleBackBarButtonItem() {
        let image = UIImage(named: "close")
        navigationBar.backIndicatorImage = image
        navigationBar.backIndicatorTransitionMaskImage = image
    }
}
