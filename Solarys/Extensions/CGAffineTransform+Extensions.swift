//
//  CGAffineTransform+Extensions.swift
//  Solarys
//
//  Created by Marcello De Palo on 11/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import Foundation
import CoreGraphics

extension CGAffineTransform {
    static var details: CGAffineTransform {
        return CGAffineTransform(translationX: 0.0, y: 0.0)
            .concatenating(CGAffineTransform(scaleX: 0.1, y: 0.1))
    }
    
    static var zero: CGAffineTransform {
        return CGAffineTransform(scaleX: 0.0, y: 0.0)
    }
}
