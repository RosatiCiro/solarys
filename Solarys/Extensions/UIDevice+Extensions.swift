//
//  UIDevice+Extensions.swift
//  Solarys
//
//  Created by Marcello De Palo on 28/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

extension UIDevice {
    var isPad: Bool {
        return (UIDevice.self.current.userInterfaceIdiom == .pad)
    }
}
