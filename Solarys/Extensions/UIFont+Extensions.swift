//
//  UIFont+Extensions.swift
//  Solarys
//
//  Created by Marcello De Palo on 22/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

extension UIFont {
    
    enum Style: String {
        case h1 = "h1"
        case h2 = "h2"
        case h3 = "h3"
        case body1 = "body1"
        case body2 = "body2"
        case small = "small"
        case `default`
    }
    
    class func `for`(style: Style) -> UIFont {
        
        func getFont(_ name: String, size: CGFloat) -> UIFont {
            return UIFont(name: name, size: size) ?? UIFont.systemFont(ofSize: size)
        }
        
        switch(style) {
        case .h1:
            return getFont("SFProDisplay-Light", size: 23)
        case .h2:
            return getFont("SFProDisplay-Thin", size: 36)
        case .h3:
            return getFont("SFProDisplay-Light", size: 31)
        case .body1:
            return getFont("SFProDisplay-Regular", size: 18)
        case .body2:
            return getFont("SFProDisplay-Medium", size: 18)
        case .small:
            return getFont("SFProDisplay-Regular", size: 12)
        case .default:
            return getFont("SFProDisplay-Regular", size: 18)
        }
    }
}
