//
//  UICollectionView+Extensions.swift
//  Solarys
//
//  Created by Marcello De Palo on 11/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

extension UICollectionView {
    
    private struct AssociatedKeys {
        static var isCentered = "UICollectionView.centered"
    }

    var isCentered: Bool {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.isCentered) as? Bool ?? false
        }
        set(offset) {
            objc_setAssociatedObject(self, &AssociatedKeys.isCentered, offset, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }

    func register<T: UICollectionViewCell>(_: T.Type) {
        register(T.self, forCellWithReuseIdentifier: T.reuseIdentifier)
    }
    
    func register<T: UICollectionViewCell>(_: T.Type) where T: NibLoadableView {
        let bundle = Bundle(for: T.self)
        let nib = UINib(nibName: T.nibName, bundle: bundle)
        
        register(nib, forCellWithReuseIdentifier: T.reuseIdentifier)
    }

    func register<T: UICollectionReusableView>(_: T.Type, viewOfKind kind: String) where T: ReusableView {
        let bundle = Bundle(for: T.self)
        let nib = UINib(nibName: String(describing: T.self), bundle: bundle)

        register(nib,
                 forSupplementaryViewOfKind: kind,
                 withReuseIdentifier: T.reuseIdentifier)
    }

    func dequeueReusableCell<T: UICollectionViewCell>(forIndexPath indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withReuseIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
            fatalError("Could not dequeue cell with identifier: \(T.reuseIdentifier)")
        }
        return cell
    }
    
    func dequeueReusable<T: UICollectionReusableView>(forIndexPath indexPath: IndexPath, viewOfKind kind: String) -> T where T: ReusableView {
        guard let view = dequeueReusableSupplementaryView(ofKind: kind,
                                                          withReuseIdentifier: T.reuseIdentifier,
                                                          for: indexPath) as? T else {
                                                            fatalError("Could not dequeue view with identifier: \(T.reuseIdentifier)")
        }
        return view
    }
}
