//
//  UIImage+Extensions.swift
//  Solarys
//
//  Created by Marcello De Palo on 11/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

extension UIImage {
    
    
    static func fromColor(_ color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) -> UIImage {
        let rect = CGRect(origin: CGPoint.zero, size: size)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context?.setFillColor(color.cgColor)
        context?.fill(rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }

    func tinted(with color: UIColor) -> UIImage? {
        UIGraphicsImageRenderer(size: size,
                                format: imageRendererFormat).image { _ in
            color.set()
            withRenderingMode(.alwaysTemplate).draw(at: .zero)
        }
    }
    
    func grayScale() -> UIImage {
        guard
            let filter: CIFilter = CIFilter(name: "CIPhotoEffectMono"),
            let ciImage = CoreImage.CIImage(image: self)
            else { return self }
        
        filter.setDefaults()
        filter.setValue(ciImage, forKey: kCIInputImageKey)

        return UIImage(cgImage: CIContext(options:nil).createCGImage(filter.outputImage!, from: filter.outputImage!.extent)!)
    }
}
