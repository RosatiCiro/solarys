//
//  String+Extensions.swift
//  Solarys
//
//  Created by Marcello De Palo on 19/12/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

extension String {
    func localized(bundle: Bundle = .main, tableName: String = "Localizable") -> String {
        return NSLocalizedString(self, tableName: tableName, value: "**\(self)**", comment: "")
    }
}
