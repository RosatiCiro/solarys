//
//  UIView+Extensions.swift
//  Solarys
//
//  Created by Marcello De Palo on 17/11/2019.
//  Copyright © 2019 Marcello De Palo. All rights reserved.
//

import UIKit

extension UIView {
    
    /// Load UIView by nib name and bundle if is not nil
    ///
    /// - Parameter nibNamed: as String
    /// - Parameter bundle: as NSBundle optional
    ///
    class func loadFromNibNamed<T: UIView>(nibNamed: String, bundle: Bundle? = nil) -> T? {
        return UINib(
            nibName: nibNamed,
            bundle: bundle
        ).instantiate(withOwner: nil, options: nil).first as? T
    }

    /// Set shadow for the view
    /// The shadow color is configurated by background color
    ///
    func shadow() {
        let cgColor: CGColor = layer.backgroundColor ?? UIColor.clear.cgColor
        
        layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        layer.shadowOpacity = 1.0
        layer.shadowRadius = 0.0
        layer.masksToBounds = false
        layer.shadowColor = UIColor(cgColor: cgColor).darker(by: 15.5).cgColor
    }    
}
