Technologies used: UIKit, CoreData, AVFoundation

The app is a quiz game about the Solar System made for kids. All the quiz structure is built upon a JSON, making the code modular and highly reusable. This was the first app published on the App Store in this academic year.

App Store Link: https://apps.apple.com/it/app/solarys/id1489101236

This was my first work in Swift. I worked more on design phase of the app as a front-end Developer, and worked a lot on my soft skills as project manager.